package com.kintreda.service.sysjob.job;

import org.springframework.stereotype.Component;

@Component("demoTask")
public class demoTask {

    public void taskWithParams(String params) {
        System.out.println("执行有参示例任务：" + params);
    }

    public void taskNoParams() {
        System.out.println("执行无参示例任务");
    }

    public void test(){
        try {
            System.out.println("---------------进来---------------");
            Thread.sleep(10000);
            System.out.println("---------------释放---------------");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
