package com.kintreda.service.config;

import com.kintreda.common.oauth.jwt.JwtConfig;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig implements WebMvcConfigurer {


    @Value("${swagger2.enable}")
    private boolean swagger2Enable;
    // 请求头Token名称
    @Value("${jwt.header}")
    private String filedName;

    SwaggerConfig(){
        this.filedName = JwtConfig.HEADER;
    }


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        registry.addResourceHandler("/swagger/**").addResourceLocations("classpath:/static/swagger/");
    }


    @Bean
    public Docket createRestPlatform() {
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(swagger2Enable)
                .groupName("后台接口")
                .apiInfo(new ApiInfoBuilder().description("后台接口").build())
                .select()
                //加了ApiOperation注解的method，才生成接口文档
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                //加了RestController注解的class，才生成接口文档
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                //包下的类，才生成接口文档
                .apis(RequestHandlerSelectors.basePackage("com.kintreda.service.platform"))
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(security())
                .securityContexts(Arrays.asList(securityContext()));
    }

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(swagger2Enable)
                .groupName("客户端接口")
                .apiInfo(new ApiInfoBuilder().description("客户端接口").build())
                .select()
                //加了ApiOperation注解的method，才生成接口文档
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                //加了RestController注解的class，才生成接口文档
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                //包下的类，才生成接口文档
                .apis(RequestHandlerSelectors.basePackage("com.kintreda.service.api"))
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(security())
                .securityContexts(Arrays.asList(securityContext()));
    }

    private List<ApiKey> security() {
        return newArrayList(
                new ApiKey(filedName, filedName, "header")
        );
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.any())
                .build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[2];
        authorizationScopes[0] = authorizationScope;
        authorizationScopes[1] = authorizationScope;
        List<SecurityReference> securityReferences = new ArrayList<>();
        securityReferences.add(new SecurityReference(filedName, authorizationScopes));
//        securityReferences.add(new SecurityReference(jwtConfig.getHeader(), authorizationScopes));
        return securityReferences;
    }

}