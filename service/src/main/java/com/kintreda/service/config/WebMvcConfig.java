package com.kintreda.service.config;

import com.kintreda.common.config.security.SecurityRequestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;


@Configuration
public class WebMvcConfig implements WebMvcConfigurer {



    private List<String> signRequestList = new SecurityRequestConfig().securityRequestList;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        GlobalInterceptor interceptor = new GlobalInterceptor();
        registry.addInterceptor(interceptor).addPathPatterns("/**").excludePathPatterns(signRequestList);
    }

}
