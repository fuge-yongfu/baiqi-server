package com.kintreda.service.platform.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.config.R;
import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import com.kintreda.common.mybatis.entity.SysProtocol;
import com.kintreda.common.mybatis.model.BasePageModel;
import com.kintreda.common.mybatis.service.ISysProtocolService;
import com.kintreda.common.oauth.annotation.SetLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/platform/protocol")
@Api(tags = "协议管理")
public class ProtocolController{

    @Autowired
    private ISysProtocolService iSysProtocolService;

    @ApiOperation(value = "协议列表")
    @GetMapping("/list")
    public R<IPage<SysProtocol>> list(BasePageModel pageModel){
        IPage<SysProtocol> protocolIPage = iSysProtocolService.managerList(pageModel);
        return R.ok(protocolIPage);
    }

    @ApiOperation(value = "保存协议")
    @PostMapping("/save")
    @SetLog("添加或修改协议")
    public R<Boolean> save(@Valid @RequestBody SysProtocol sysProtocol){
        sysProtocol.setUpdateTime(LocalDateTime.now());
        iSysProtocolService.saveOrUpdate(sysProtocol);
        return R.ok();
    }

    @ApiOperation(value = "删除协议")
    @DeleteMapping("/del/{protocolId}")
    @SetLog("删除协议")
    public R<Boolean> del(@PathVariable Integer protocolId){
        try {
            iSysProtocolService.removeById(protocolId);
            return R.ok(protocolId);
        }catch (Exception e){
            throw new CommonBaseException(CommonBaseErrorCode.DEL_FAIL);
        }
    }

}
