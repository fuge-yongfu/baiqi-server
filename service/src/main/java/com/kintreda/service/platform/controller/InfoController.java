package com.kintreda.service.platform.controller;

import com.kintreda.common.config.R;
import com.kintreda.common.config.enums.UserTypeEnum;
import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import com.kintreda.common.mybatis.entity.SysAdmin;
import com.kintreda.common.mybatis.service.ISysAdminService;
import com.kintreda.common.oauth.annotation.SetLog;
import com.kintreda.common.oauth.jwt.JwtUtils;
import com.kintreda.tools.CommonUtils;
import com.kintreda.tools.upload.UploadUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/platform/info")
@Api(tags = "基本信息")
public class InfoController {

    @Autowired
    private ISysAdminService iSysAdminService;

    @GetMapping("/reset-password")
    @ApiOperation(value = "重置登录密码")
    @SetLog("重置登录密码")
    public R<Boolean> resetPassword(String password, HttpServletRequest request){
        int userId = JwtUtils.getUserId(request);
        String userType = JwtUtils.getUserType(request);
        if (userType.equals(UserTypeEnum.后台管理员.getType())){
            SysAdmin sysAdmin = iSysAdminService.getById(userId);
            sysAdmin.setPassword(CommonUtils.getPwd(password));
            iSysAdminService.updateById(sysAdmin);
            return R.ok();
        }else{
            throw new CommonBaseException(CommonBaseErrorCode.ACCOUNT_NOT_AUTH);
        }
    }

    @ApiOperation(value = "图片上传")
    @PostMapping("/upload")
    public R upImg(@ApiParam(name = "file",value = "图片文件") @RequestParam("file") MultipartFile file){
        try {
            String result = UploadUtils.doUp(file);
            return R.ok(result);
        }catch (CommonBaseException e){
            throw e;
        }catch (Exception e){
            throw new CommonBaseException(CommonBaseErrorCode.USER_PICTURE_FAIL);
        }
    }

}
