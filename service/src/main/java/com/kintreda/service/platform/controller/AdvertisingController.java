package com.kintreda.service.platform.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.config.R;
import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import com.kintreda.common.mybatis.entity.SysAdPosition;
import com.kintreda.common.mybatis.entity.SysAdvertising;
import com.kintreda.common.mybatis.model.AdvertisingModel;
import com.kintreda.common.mybatis.service.ISysAdPositionService;
import com.kintreda.common.mybatis.service.ISysAdvertisingService;
import com.kintreda.common.oauth.annotation.SetLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/platform/advertising")
@Api(tags = "广告管理")
public class AdvertisingController {

    @Autowired
    private ISysAdvertisingService iSysAdvertisingService;
    @Autowired
    private ISysAdPositionService iSysAdPositionService;

    @ApiOperation(value = "广告列表")
    @GetMapping("/list")
    public R<IPage<SysAdvertising>> list(AdvertisingModel param){
        IPage<SysAdvertising>  result = iSysAdvertisingService.managerPage(param);
        return R.ok(result);
    }

    @ApiOperation(value = "保存广告")
    @PostMapping("/save")
    @SetLog("添加或修改广告")
    public R<Boolean> save(@Valid @RequestBody SysAdvertising data){
        iSysAdvertisingService.saveOrUpdate(data);
        return R.ok();
    }

    @ApiOperation(value = "删除广告")
    @DeleteMapping("/del/{adId}")
    @SetLog("删除广告")
    public R<Boolean> del(@PathVariable Integer adId){
        try {
            iSysAdvertisingService.removeById(adId);
            return R.ok();
        }catch (Exception e){
            throw new CommonBaseException(CommonBaseErrorCode.DEL_FAIL);
        }
    }

    @ApiOperation(value = "广告位置")
    @GetMapping("/position")
    public R<List<SysAdPosition>> position(){
        List<SysAdPosition> positionList = iSysAdPositionService.getAll();
        return R.ok(positionList);
    }

    @ApiOperation(value = "保存广告位置")
    @PostMapping("/position/save")
    public R<Boolean> savePosition(@Valid @RequestBody SysAdPosition data){
        iSysAdPositionService.saveOrUpdate(data);
        return R.ok(data);
    }

    @ApiOperation(value = "删除广告位")
    @DeleteMapping("/position/del/{positionId}")
    @SetLog("删除广告位")
    public R<Boolean> delPosition(@PathVariable Integer positionId){
        try {
            iSysAdPositionService.removeById(positionId);
            return R.ok();
        }catch (Exception e){
            throw new CommonBaseException(CommonBaseErrorCode.DEL_FAIL);
        }
    }

}
