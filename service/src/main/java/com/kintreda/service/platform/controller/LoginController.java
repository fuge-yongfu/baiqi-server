package com.kintreda.service.platform.controller;

import com.anji.captcha.model.common.ResponseModel;
import com.anji.captcha.model.vo.CaptchaVO;
import com.anji.captcha.service.CaptchaService;
import com.kintreda.common.config.R;
import com.kintreda.common.config.enums.UserTypeEnum;
import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import com.kintreda.common.mybatis.entity.SysAdmin;
import com.kintreda.common.mybatis.entity.SysRole;
import com.kintreda.common.mybatis.service.ISysAdminService;
import com.kintreda.common.mybatis.service.ISysRoleMenuService;
import com.kintreda.common.mybatis.service.ISysRoleService;
import com.kintreda.common.mybatis.vos.TreeMenuVo;
import com.kintreda.common.oauth.jwt.JwtUtils;
import com.kintreda.service.platform.model.LoginModel;
import com.kintreda.service.platform.model.LoginVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/platform/login")
@Api(tags = "登录相关")
public class LoginController {

    private String group = "platform";
    @Autowired
    private ISysAdminService iSysAdminService;
    @Autowired
    private ISysRoleMenuService iSysRoleMenuService;
    @Autowired
    private ISysRoleService iSysRoleService;
    @Autowired
    private CaptchaService captchaService;

    @PostMapping
    @ApiOperation(value = "登录接口")
    public R<LoginVo> doLogin(@Valid @RequestBody LoginModel param){
        CaptchaVO captchaVO = new CaptchaVO();
        captchaVO.setCaptchaVerification(param.getToken());
        ResponseModel response = captchaService.verification(captchaVO);
        if (!response.isSuccess()){
            throw new CommonBaseException(CommonBaseErrorCode.SYS_ERROR.setErrMsg(response.getRepMsg()).setCode(Integer.valueOf(response.getRepCode())));
        }
        SysAdmin sysAdmin = iSysAdminService.login(param.getUsername(),param.getPassword());
        if (ObjectUtils.isEmpty(sysAdmin)  || !new BCryptPasswordEncoder().matches(param.getPassword(),sysAdmin.getPassword())){
            throw new CommonBaseException(CommonBaseErrorCode.INCORRECT_USERNAME_OR_PASSWORD);
        }
        if (sysAdmin.getStatus()!=1){
            throw new CommonBaseException(CommonBaseErrorCode.ACCOUNT_LOGIN_CLOSE);
        }
        //获取角色信息
        SysRole role = iSysRoleService.getById(sysAdmin.getRoleId());
        if (ObjectUtils.isEmpty(role) || role.getStatus()!=1){
            throw new CommonBaseException(CommonBaseErrorCode.ACCOUNT_LOGIN_CLOSE);
        }
        String token = JwtUtils.createToken(sysAdmin.getAdmId().toString(), UserTypeEnum.后台管理员.getType());
        LoginVo loginVo = new LoginVo();
        loginVo.setRoleId(role.getRoleId());
        loginVo.setRoleName(role.getName());
        loginVo.setToken(token);
        loginVo.setAdmId(sysAdmin.getAdmId());
        loginVo.setName(sysAdmin.getName());
        loginVo.setExpireDate(JwtUtils.getTheExpirationTime(token));
        //获取权限列表
        List<String> roleVos = iSysRoleMenuService.getRolesByRoleId(sysAdmin.getRoleId(),group);
        if (ObjectUtils.isEmpty(roleVos) || roleVos.size()<1){
            throw new CommonBaseException(CommonBaseErrorCode.NOT_AUTH);
        }
        //获取角色对应的树形菜单
        List<TreeMenuVo> treeMenuVos = iSysRoleMenuService.menuTreeByRoleId(sysAdmin.getRoleId(),group);
        loginVo.setMenu(treeMenuVos);
        loginVo.setRole(roleVos);
        return R.ok(loginVo);
    }

}
