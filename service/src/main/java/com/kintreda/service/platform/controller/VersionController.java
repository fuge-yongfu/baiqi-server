package com.kintreda.service.platform.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.config.R;
import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import com.kintreda.common.mybatis.entity.SysVersion;
import com.kintreda.common.mybatis.model.VersionModel;
import com.kintreda.common.mybatis.service.ISysVersionService;
import com.kintreda.common.oauth.annotation.SetLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/platform/version")
@Api(tags = "版本管理")
public class VersionController {

    @Autowired
    private ISysVersionService iSysVersionService;

    @ApiOperation(value = "版本列表")
    @GetMapping("/list")
    public R<IPage<SysVersion>> list(VersionModel param){
        IPage<SysVersion> result = iSysVersionService.managerPage(param);
        return R.ok(result);
    }

    @ApiOperation(value = "保存版本")
    @PostMapping("/save")
    @SetLog("添加或修改客户端版本数据")
    public R<Boolean> save(@Valid @RequestBody SysVersion data){
        data.setUpdateTime(LocalDateTime.now());
        iSysVersionService.saveOrUpdate(data);
        return R.ok();
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/del/{id}")
    @SetLog("删除版本信息")
    public R<Boolean> del(@PathVariable Integer id){
        try {
            iSysVersionService.removeById(id);
            return R.ok();
        }catch (Exception e){
            throw new CommonBaseException(CommonBaseErrorCode.DEL_FAIL);
        }
    }

}
