package com.kintreda.service.platform.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kintreda.common.config.R;
import com.kintreda.common.mybatis.entity.SysLog;
import com.kintreda.common.mybatis.model.BasePageModel;
import com.kintreda.common.mybatis.service.ISysLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kotlin.jvm.internal.Lambda;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.function.Consumer;

@RestController
@RequestMapping("/platform/logs")
@Api(tags = "操作日志")
public class LogsController {

    @Resource
    private ISysLogService iSysLogService;

    @GetMapping("/list")
    @ApiOperation(value = "操作日志列表")
    public R<IPage<SysLog>> list(BasePageModel param) {
        IPage<SysLog> page = new Page<>(param.getPage(), param.getLimit());
        LambdaQueryWrapper<SysLog> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(param.getKeyword())) {
            queryWrapper.and(lambda -> {
                lambda.like(SysLog::getUsername, param.getKeyword()).or().like(SysLog::getOperation, param.getKeyword());
            });
        }
        if (StringUtils.isNotBlank(param.getDate())) {
            queryWrapper.like(SysLog::getCreateTime, param.getDate());
        }
        if (StringUtils.isNotBlank(param.getStartDate())) {
            queryWrapper.gt(SysLog::getCreateTime, LocalDate.parse(param.getStartDate()).minusDays(1));
        }
        if (StringUtils.isNotBlank(param.getEndDate())) {
            queryWrapper.lt(SysLog::getCreateTime, LocalDate.parse(param.getEndDate()).plusDays(1));
        }
        queryWrapper.orderByDesc(SysLog::getId);
        IPage<SysLog> result = iSysLogService.page(page, queryWrapper);
        return R.ok(result);
    }

}
