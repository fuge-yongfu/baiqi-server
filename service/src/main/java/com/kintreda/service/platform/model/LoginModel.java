package com.kintreda.service.platform.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "登录参数")
public class LoginModel {

    @ApiModelProperty(value = "用户名",required = true)
    @NotBlank(message = "请输入用户名")
    private String username;

    @ApiModelProperty(value = "密码",required = true)
    @NotBlank(message = "请输入密码")
    private String password;

    @ApiModelProperty(value = "token")
    private String token;

    @ApiModelProperty(value = "authenticate")
    private String authenticate;

}
