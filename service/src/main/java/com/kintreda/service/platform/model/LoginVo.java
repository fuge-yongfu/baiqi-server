package com.kintreda.service.platform.model;

import com.kintreda.common.mybatis.vos.TreeMenuVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 登录返回对象
 */
@Data
@ApiModel("登录返回对象")
public class LoginVo {

    @ApiModelProperty(value = "AdmId")
    private Integer admId;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "角色Id")
    private Integer roleId;

    @ApiModelProperty(value = "token")
    private String token;

    @ApiModelProperty(value = "过期时间")
    private Date expireDate;

    @ApiModelProperty(value = "菜单")
    private List<TreeMenuVo> menu = new ArrayList<>();

    @ApiModelProperty(value = "权限")
    private List<String> role = new ArrayList<>();

}
