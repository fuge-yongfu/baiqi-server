package com.kintreda.service.platform.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.config.R;
import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import com.kintreda.common.mybatis.entity.SysFeedback;
import com.kintreda.common.mybatis.model.FeedbackModel;
import com.kintreda.common.mybatis.service.ISysFeedbackService;
import com.kintreda.common.oauth.annotation.SetLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/platform/feedback")
@Api(tags = "意见反馈")
public class FeedbackController {

    @Autowired
    private ISysFeedbackService iSysFeedbackService;

    @ApiOperation(value = "反馈数据")
    @GetMapping("/list")
    public R<IPage<SysFeedback>> list(FeedbackModel param){
        IPage<SysFeedback> sysFeedbackIPage = iSysFeedbackService.managerPage(param);
        return R.ok(sysFeedbackIPage);
    }

    @ApiOperation(value = "回复")
    @GetMapping("/replay/{id}")
    @SetLog("反馈内容回复")
    public R<Boolean> replay(@PathVariable Integer id,String replayContent){
        SysFeedback feedback = iSysFeedbackService.getById(id);
        if (ObjectUtils.isEmpty(feedback)){
            throw new CommonBaseException(CommonBaseErrorCode.DATA_IS_NULL);
        }
        feedback.setReplay(replayContent);
        iSysFeedbackService.updateById(feedback);
        return R.ok();
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/del/{id}")
    @SetLog("删除反馈数据")
    public R<Boolean> del(@PathVariable Integer id){
        try {
            iSysFeedbackService.removeById(id);
            return R.ok();
        }catch (Exception e){
            throw new CommonBaseException(CommonBaseErrorCode.DEL_FAIL);
        }
    }

}
