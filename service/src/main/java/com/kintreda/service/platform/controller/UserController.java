package com.kintreda.service.platform.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.config.R;
import com.kintreda.common.mybatis.entity.SysUser;
import com.kintreda.common.mybatis.model.UserPageModel;
import com.kintreda.common.mybatis.service.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/platform/user")
@Api(tags = "用户模块")
public class UserController {

    @Autowired
    private ISysUserService iSysUserService;
    @ApiOperation(value = "用户管理")
    @GetMapping("/list")
    public R<IPage<SysUser>> userPage(UserPageModel param){
        IPage<SysUser> userIPage = iSysUserService.listByManager(param);
        return R.ok(userIPage);
    }



}
