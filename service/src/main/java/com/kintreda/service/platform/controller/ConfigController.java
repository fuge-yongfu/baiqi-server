package com.kintreda.service.platform.controller;

import com.kintreda.common.config.R;
import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import com.kintreda.common.mybatis.entity.SysConfig;
import com.kintreda.common.mybatis.model.ConfigGroupModel;
import com.kintreda.common.mybatis.service.ISysConfigService;
import com.kintreda.common.oauth.annotation.SetLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/platform/config")
@Api(tags = "配置中心")
public class ConfigController {

    @Resource
    private ISysConfigService iSysConfigService;

    @GetMapping("/list")
    @ApiOperation(value = "获取配置列表")
    public R<List<ConfigGroupModel>> configList(){
        return iSysConfigService.allConfig();
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存配置")
    @SetLog("修改设置信息")
    public R<Boolean> doSave(@RequestBody List<SysConfig> params){
        try {
            iSysConfigService.updateBatchById(params);
            //更新缓存
            iSysConfigService.createCache();
            return R.ok();
        }catch (Exception e){
            e.printStackTrace();
            throw new CommonBaseException(CommonBaseErrorCode.SYS_ERROR);
        }
    }

}
