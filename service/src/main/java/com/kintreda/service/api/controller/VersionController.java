package com.kintreda.service.api.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kintreda.common.config.R;
import com.kintreda.common.mybatis.entity.SysVersion;
import com.kintreda.common.mybatis.service.ISysVersionService;
import com.kintreda.service.api.ApiBaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("VersionController")
@Api(tags = "版本管理")
public class VersionController extends ApiBaseController {

    @Autowired
    private ISysVersionService iSysVersionService;

    @ApiOperation(value = "获取客户端最新版本")
    @GetMapping("/version/new/{platform}")
    @ApiImplicitParam(name = "platform",value = "平台类型：android | ios | pc",required = true)
    public R<SysVersion> androidNew(@PathVariable String platform){
        QueryWrapper<SysVersion> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("platform",platform);
        queryWrapper.eq("status",1);
        queryWrapper.orderByDesc("version");
        queryWrapper.last("limit 1");
        List<SysVersion> list = iSysVersionService.list(queryWrapper);
        if (ObjectUtils.isNotEmpty(list) && list.size()>0){
            return R.ok(list.get(0));
        }else{
            return R.ok(null);
        }
    }


}
