package com.kintreda.service.api.controller;

import com.kintreda.common.config.R;
import com.kintreda.common.mybatis.entity.SysUser;
import com.kintreda.common.mybatis.service.ISysUserService;
import com.kintreda.service.api.ApiBaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController("InfoController")
@Api(tags = "基本信息")
public class InfoController extends ApiBaseController {

    @Autowired
    private ISysUserService iSysUserService;

    @ApiOperation("获取基本信息")
    @GetMapping("/info")
    public R<SysUser> getInfo(){
        SysUser user = getUser();
        return R.ok(user);
    }


}
