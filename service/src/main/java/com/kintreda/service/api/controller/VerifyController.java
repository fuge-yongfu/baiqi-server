package com.kintreda.service.api.controller;

import com.anji.captcha.controller.CaptchaController;
import com.anji.captcha.model.common.ResponseModel;
import com.anji.captcha.model.vo.CaptchaVO;
import com.anji.captcha.service.CaptchaService;
import com.kintreda.service.api.service.VerifyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/verify")
@Api(tags = "验证码")
public class VerifyController extends CaptchaController {

    @Autowired
    private CaptchaService captchaService;
    @Autowired
    private VerifyService verifyService;


    @ApiOperation("获取参数")
    @PostMapping({"/get"})
    @Override
    public ResponseModel get(@RequestBody CaptchaVO data, HttpServletRequest request) {
        assert request.getRemoteHost() != null;
        data.setBrowserInfo(getRemoteId(request));
        return this.captchaService.get(data);
    }

    /**
     * 验证数据
     * @param data
     * @param request
     * @return
     */
    @ApiOperation(value = "数据验证")
    @PostMapping({"/check"})
    @Override
    public ResponseModel check(@RequestBody CaptchaVO data, HttpServletRequest request) {
        data.setBrowserInfo(getRemoteId(request));
        return this.captchaService.check(data);
    }


//    @ApiOperation("二次效验")
//    @PutMapping("/verify")
//    public R verify(@RequestParam("token") String token) {
//       return verifyService.verify(token);
//    }


}
