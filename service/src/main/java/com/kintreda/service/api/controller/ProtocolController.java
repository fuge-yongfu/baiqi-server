package com.kintreda.service.api.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kintreda.common.config.R;
import com.kintreda.common.mybatis.entity.SysProtocol;
import com.kintreda.common.mybatis.service.ISysProtocolService;
import com.kintreda.service.api.ApiBaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController("ProtocolController")
@Api(tags = "协议相关")
public class ProtocolController extends ApiBaseController {

    @Autowired
    private ISysProtocolService iSysProtocolService;

    @ApiOperation(value = "协议标识")
    @GetMapping("/protocol/label")
    public R<List<Map<String,String>>> label(){
        List<Map<String,String>> result = new ArrayList<>();
        List<SysProtocol> list = iSysProtocolService.list();
        if (ObjectUtils.isNotEmpty(list)){
            Iterator<SysProtocol> iterator = list.iterator();
            while (iterator.hasNext()){
                SysProtocol current = iterator.next();
                Map<String,String> item = new HashMap<>();
                item.put("name",current.getTitle());
                item.put("protocol",current.getProtocol());
                result.add(item);
            }
        }
        return R.ok(result);
    }

    @ApiOperation(value = "协议详情")
    @GetMapping("/protocol/detail/{protocol}")
    @ApiImplicitParam(name = "protocol",value = "标识",required = true)
    public R<SysProtocol> detail(@PathVariable String protocol){
        QueryWrapper<SysProtocol> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("protocol",protocol);
        SysProtocol one = iSysProtocolService.getOne(queryWrapper);
        return R.ok(one);
    }



}
