package com.kintreda.service.api.service;

import com.kintreda.common.config.enums.UserTypeEnum;
import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import com.kintreda.common.mybatis.entity.SysRole;
import com.kintreda.common.mybatis.entity.SysUser;
import com.kintreda.common.mybatis.service.ISysRoleMenuService;
import com.kintreda.common.mybatis.service.ISysRoleService;
import com.kintreda.common.mybatis.service.ISysUserService;
import com.kintreda.common.oauth.jwt.JwtUtils;
import com.kintreda.tools.wxmp.cmd.UserInfoModel;
import com.kintreda.tools.wxmp.cmd.WeixinMpLoginVo;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Component
public class LoginService {
    @Value("${spring.application.name}")
    private String group;
    @Autowired
    private ISysUserService iSysUserService;
    @Autowired
    private ISysRoleService iSysRoleService;
    @Autowired
    private ISysRoleMenuService iSysRoleMenuService;
    /**
     * 小程序登录
     * @param userInfo
     * @return
     */
    public WeixinMpLoginVo mpLogin(@Valid UserInfoModel userInfo) {
        SysUser sysUser = iSysUserService.getByWxmpOpenId(userInfo.getOpenId());
        if (ObjectUtils.isNotEmpty(sysUser) && sysUser.getStatus()!=1){
            throw new CommonBaseException(CommonBaseErrorCode.ACCOUNT_LOGIN_CLOSE);
        }
        if (ObjectUtils.isEmpty(sysUser)){
            sysUser = new SysUser();
            sysUser.setRegisterDate(LocalDate.now());
        }
        sysUser.setAvatar(userInfo.getAvatarUrl());
        sysUser.setNickName(userInfo.getNickName());
        sysUser.setWxmpOpenId(userInfo.getOpenId());
        sysUser.setSex(userInfo.getGender());
        sysUser.setStatus(1);
        sysUser.setRoleId(UserTypeEnum.用户.getRoleId());
        iSysUserService.saveOrUpdate(sysUser);
        return loginSuccess(sysUser);
    }

    /**
     * openid直接登录
     * @param openId
     * @return
     */
    public WeixinMpLoginVo openLogin(String openId) {
        SysUser sysUser = iSysUserService.getByWxmpOpenId(openId);
        if (ObjectUtils.isNotEmpty(sysUser) && sysUser.getStatus()!=1){
            throw new CommonBaseException(CommonBaseErrorCode.ACCOUNT_LOGIN_CLOSE);
        }
        if (ObjectUtils.isEmpty(sysUser)){
            throw new CommonBaseException(CommonBaseErrorCode.USER_LOGIN_FAIL);
        }
        sysUser.setStatus(1);
        return loginSuccess(sysUser);
    }

    /**
     * 登录成功返回信息
     * @param sysUser
     * @return
     */
    private WeixinMpLoginVo loginSuccess(SysUser sysUser) {
        //定义返回对象
        WeixinMpLoginVo loginVo = new WeixinMpLoginVo();
        loginVo.setNickName(sysUser.getNickName());
        loginVo.setAvatar(sysUser.getAvatar());
        loginVo.setUserId(sysUser.getUserId());

        //获取角色信息
        SysRole role = iSysRoleService.getById(sysUser.getRoleId());
        loginVo.setRoleId(role.getRoleId());
        loginVo.setRoleName(role.getName());
        //获取权限
        List<Map<String,Object>> roles = iSysRoleMenuService.getRolesListByRoleId(role.getRoleId(), group);
        loginVo.setRoles(roles);
        //生成token
        String token = JwtUtils.createToken(sysUser.getUserId().toString(), UserTypeEnum.用户.getType(),720L);
        loginVo.setToken(token);
        return loginVo;
    }

}
