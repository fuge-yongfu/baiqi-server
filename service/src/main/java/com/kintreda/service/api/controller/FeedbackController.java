package com.kintreda.service.api.controller;

import com.kintreda.common.config.R;
import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import com.kintreda.common.mybatis.entity.SysFeedback;
import com.kintreda.common.mybatis.service.ISysFeedbackService;
import com.kintreda.service.api.ApiBaseController;
import com.kintreda.service.api.service.VerifyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDateTime;

@RestController("ApiFeedbackController")
@Api(tags = "意见反馈")
public class FeedbackController extends ApiBaseController {

    @Autowired
    private ISysFeedbackService iSysFeedbackService;
    @Autowired
    private VerifyService verifyService;

    @ApiOperation(value = "反馈提交")
    @PostMapping("/feedback/post")
    public R<Boolean> doPost(@Valid @RequestBody SysFeedback data){
        if (ObjectUtils.isEmpty(data.getToken())){
            throw new CommonBaseException(CommonBaseErrorCode.SYS_VERIFY_ERROR);
        }
        R verify = verifyService.verify(data.getToken());
        if (!verify.getCode().equals(200)){
            throw new CommonBaseException(CommonBaseErrorCode.SYS_ERROR.setCode(verify.getCode()).setErrMsg(verify.getMessage()));
        }
        data.setCreateTime(LocalDateTime.now());
        boolean save = iSysFeedbackService.save(data);
        return R.ok(save);
    }

}
