package com.kintreda.service.api.service;

import com.anji.captcha.model.common.ResponseModel;
import com.anji.captcha.model.vo.CaptchaVO;
import com.anji.captcha.service.CaptchaService;
import com.kintreda.common.config.R;
import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VerifyService {

    @Autowired
    private CaptchaService captchaService;

    /**
     * 验证码二次效验
     * @param token
     * @return
     */
    public R verify(String token){
        CaptchaVO captchaVO = new CaptchaVO();
        captchaVO.setCaptchaVerification(token);
        ResponseModel verification = this.captchaService.verification(captchaVO);
        if (!verification.isSuccess()){
            throw new CommonBaseException(CommonBaseErrorCode.SYS_VERIFY_ERROR.setErrMsg(verification.getRepMsg()).setCode(Integer.valueOf(verification.getRepCode())));
        }
        return R.ok();
    }

}
