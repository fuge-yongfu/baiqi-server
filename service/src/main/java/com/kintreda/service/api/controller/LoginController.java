package com.kintreda.service.api.controller;

import com.kintreda.common.config.R;
import com.kintreda.service.api.ApiBaseController;
import com.kintreda.service.api.service.LoginService;
import com.kintreda.tools.wxmp.WeixinMpApi;
import com.kintreda.tools.wxmp.cmd.UserInfoModel;
import com.kintreda.tools.wxmp.cmd.WeixinMpLoginVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController("LoginController")
@Api(tags = "用户登录")
public class LoginController extends ApiBaseController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private WeixinMpApi weixinMpApi;

    @ApiOperation(value = "获取会话密钥")
    @GetMapping("/login/session/{code}")
    public R<String> session(@PathVariable String code){
        String openId = weixinMpApi.createSession(code);
        return R.ok(openId);
    }

    @ApiOperation(value = "小程序登录")
    @PostMapping("/login/mpLogin")
    public R<WeixinMpLoginVo> mpLogin(@Valid @RequestBody UserInfoModel userInfo){
        WeixinMpLoginVo weixinMpLoginVo = loginService.mpLogin(userInfo);
        return R.ok(weixinMpLoginVo);
    }

    @ApiOperation(value = "openId登录")
    @PostMapping("/login/openIdLogin")
    public R<WeixinMpLoginVo> login(String openId){
        WeixinMpLoginVo loginVo = loginService.openLogin(openId);
        return R.ok(loginVo);
    }


}
