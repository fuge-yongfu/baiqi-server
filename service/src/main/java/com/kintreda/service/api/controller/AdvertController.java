package com.kintreda.service.api.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kintreda.common.config.R;
import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import com.kintreda.common.mybatis.entity.SysAdPosition;
import com.kintreda.common.mybatis.entity.SysAdvertising;
import com.kintreda.common.mybatis.service.ISysAdPositionService;
import com.kintreda.common.mybatis.service.ISysAdvertisingService;
import com.kintreda.service.api.ApiBaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("AdvertController")
@Api(tags = "广告相关")
public class AdvertController extends ApiBaseController {

    @Autowired
    private ISysAdvertisingService iSysAdvertisingService;
    @Autowired
    private ISysAdPositionService iSysAdPositionService;

    @ApiOperation(value = "广告位置 ")
    @GetMapping("/advert/position")
    public R<List<SysAdPosition>> position(){
        List<SysAdPosition> list = iSysAdPositionService.list();
        return R.ok(list);
    }

    @ApiOperation(value = "广告图片")
    @GetMapping("/advert/{positionId}")
    @ApiImplicitParams({
            @ApiImplicitParam(value = "位置Id",name = "positionId",required = true)
    })
    public R<List<SysAdvertising>> list(@PathVariable Integer positionId){
        //获取位置信息
        SysAdPosition position = iSysAdPositionService.getById(positionId);
        if (ObjectUtils.isEmpty(position)){
            throw new CommonBaseException(CommonBaseErrorCode.DATA_IS_NULL);
        }
        //获取广告列表
        QueryWrapper<SysAdvertising> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("position_id",position.getId());
        queryWrapper.eq("status",1);
        queryWrapper.orderByDesc("sort");
        queryWrapper.last("limit "+position.getNum());
        List<SysAdvertising> list = iSysAdvertisingService.list(queryWrapper);

        return R.ok(list);
    }

}
