package com.kintreda.service.api;

import com.kintreda.common.mybatis.entity.SysUser;
import com.kintreda.common.oauth.jwt.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api")
public class ApiBaseController {

    @Autowired
    protected HttpServletRequest request;

    /**
     * 获取登录账户类型
     * @return
     */
    protected String getUserType(){
        return JwtUtils.getUserType(request);
    }

    /**
     * 获取登录用户信息
     * @return
     */
    protected SysUser getUser(){
        return JwtUtils.getCurrentUserInfo(request, SysUser.class);
    }

}
