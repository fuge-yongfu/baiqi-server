# ************************************************************
# Sequel Ace SQL dump
# 版本号： 20062
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# 主机: 127.0.0.1 (MySQL 5.7.40-log)
# 数据库: baishou
# 生成时间: 2024-01-26 07:52:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE='NO_AUTO_VALUE_ON_ZERO', SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# 转储表 sys_ad_position
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_ad_position`;

CREATE TABLE `sys_ad_position` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL DEFAULT '' COMMENT '位置名称',
  `num` int(10) NOT NULL DEFAULT '5' COMMENT '获取数量',
  `width` int(10) DEFAULT NULL COMMENT '图片宽度',
  `height` int(10) DEFAULT NULL COMMENT '图片高度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='广告位置';

LOCK TABLES `sys_ad_position` WRITE;
/*!40000 ALTER TABLE `sys_ad_position` DISABLE KEYS */;

INSERT INTO `sys_ad_position` (`id`, `name`, `num`, `width`, `height`)
VALUES
	(1,'BANNER',5,750,360);

/*!40000 ALTER TABLE `sys_ad_position` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_admin`;

CREATE TABLE `sys_admin` (
  `adm_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '姓名',
  `username` varchar(80) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(128) NOT NULL DEFAULT '' COMMENT '密码',
  `role_id` int(11) NOT NULL COMMENT '所属角色',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`adm_id`),
  UNIQUE KEY `username` (`username`),
  KEY `status` (`status`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='管理人员';

LOCK TABLES `sys_admin` WRITE;
/*!40000 ALTER TABLE `sys_admin` DISABLE KEYS */;

INSERT INTO `sys_admin` (`adm_id`, `name`, `username`, `password`, `role_id`, `status`)
VALUES
	(1,'管理员','admin','$2a$10$PnyparKTQ43m7Eyo70QBpO7qFYu3.08ryNau6LTEw0FbmUjaF58Xe',1,1);

/*!40000 ALTER TABLE `sys_admin` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_advertising
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_advertising`;

CREATE TABLE `sys_advertising` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(300) DEFAULT NULL COMMENT '广告名称',
  `position_id` int(10) NOT NULL COMMENT '所属位置',
  `src` varchar(300) NOT NULL DEFAULT '' COMMENT '图片地址',
  `link` varchar(300) DEFAULT '' COMMENT '跳转地址',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` tinyint(2) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `position_id` (`position_id`),
  KEY `status` (`status`),
  KEY `sort` (`sort`),
  KEY `title` (`title`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='广告';

LOCK TABLES `sys_advertising` WRITE;
/*!40000 ALTER TABLE `sys_advertising` DISABLE KEYS */;

INSERT INTO `sys_advertising` (`id`, `title`, `position_id`, `src`, `link`, `sort`, `status`)
VALUES
	(1,'第一张',1,'http://static.lt.kintreda.com/3fa14ea9af844ff29ca83977bf3553cd.jpeg','1',0,1);

/*!40000 ALTER TABLE `sys_advertising` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_config`;

CREATE TABLE `sys_config` (
  `config_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '参数名称',
  `value` mediumtext COLLATE utf8mb4_unicode_ci COMMENT '参数值',
  `grouping` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '分组',
  `type` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'input' COMMENT '设置形式',
  `length` int(2) NOT NULL DEFAULT '8' COMMENT '配置长度',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `is_show` tinyint(1) DEFAULT '1' COMMENT '是否显示',
  `options` text COLLATE utf8mb4_unicode_ci COMMENT '选项信息',
  `rules` text COLLATE utf8mb4_unicode_ci COMMENT '验证规则',
  `remark` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '参数说明',
  PRIMARY KEY (`config_id`),
  UNIQUE KEY `name` (`name`),
  KEY `grouping` (`grouping`),
  KEY `is_show` (`is_show`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='配置信息';

LOCK TABLES `sys_config` WRITE;
/*!40000 ALTER TABLE `sys_config` DISABLE KEYS */;

INSERT INTO `sys_config` (`config_id`, `title`, `name`, `value`, `grouping`, `type`, `length`, `sort`, `is_show`, `options`, `rules`, `remark`)
VALUES
	(1,'系统名称','sys_name','白手起架','SYSTEM','input',4,100,1,'','{\"rules\":[{\"required\":true,\"message\":\"请输入系统名称\"}],\"disabled\":false,\"readonly\":false}',''),
	(2,'版权信息','copy_right','Prower © 2024 Created by 白手起架','SYSTEM','textarea',12,0,1,'','{\"rules\":[]}',''),
	(3,'用户注册','user_register','1','SYSTEM','select',4,90,1,'[{\"value\":\"0\",\"name\":\"暂停新用户注册\"},{\"value\":\"1\",\"name\":\"允许新用户注册\"}]','{\"rules\":[{\"required\":true}]}',''),
	(4,'营业时间','times','[\"08:00\",\"21:00\"]','SYSTEM','time-range',4,99,1,'','{\"rules\":[{\"required\":true,\"message\":\"请选择营业时间\"}],\"disabled\":false,\"readonly\":false}',''),
	(5,'客服电话','service_tel','400-0000-0000','SYSTEM','input',4,98,1,'','{\"rules\":[{\"required\":true}]}','');

/*!40000 ALTER TABLE `sys_config` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_feedback
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_feedback`;

CREATE TABLE `sys_feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL DEFAULT '' COMMENT '姓名',
  `phone` varchar(80) NOT NULL DEFAULT '' COMMENT '手机号',
  `content` varchar(500) NOT NULL DEFAULT '' COMMENT '反馈内容',
  `create_time` datetime NOT NULL COMMENT '反馈时间',
  `replay` varchar(500) DEFAULT NULL COMMENT '回复内容',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `phone` (`phone`),
  KEY `create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='反馈数据';



# 转储表 sys_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_log`;

CREATE TABLE `sys_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `service` varchar(120) NOT NULL DEFAULT '' COMMENT '服务名称',
  `username` varchar(120) DEFAULT NULL COMMENT '操作人',
  `operation` varchar(120) NOT NULL DEFAULT '' COMMENT '操作',
  `method` varchar(120) NOT NULL DEFAULT '' COMMENT '方法名称',
  `params` mediumtext COMMENT '参数',
  `ip` varchar(120) DEFAULT NULL COMMENT 'ip',
  `create_time` datetime NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='操作日志';

LOCK TABLES `sys_log` WRITE;
/*!40000 ALTER TABLE `sys_log` DISABLE KEYS */;

INSERT INTO `sys_log` (`id`, `service`, `username`, `operation`, `method`, `params`, `ip`, `create_time`)
VALUES
	(1,'service-platform','管理员','重置登录密码','com.kintreda.service.platform.controller.InfoController.resetPassword','参数转换错误,可能是无参数请求!','10.10.10.10','2021-01-26 15:06:30'),
	(2,'service-platform','管理员','删除权限菜单','com.kintreda.service.platform.controller.MenuController.del','[41]','10.10.10.10','2021-01-26 15:10:33'),
	(3,'service-platform','管理员','删除权限菜单','com.kintreda.service.platform.controller.MenuController.del','[40]','10.10.10.10','2021-01-26 15:10:37'),
	(10,'service','管理员','修改设置信息','com.kintreda.service.platform.controller.ConfigController.doSave','[[{\"configId\":1,\"grouping\":\"SYSTEM\",\"isShow\":true,\"length\":4,\"name\":\"sys_name\",\"options\":\"\",\"remark\":\"\",\"rules\":\"{\\\"rules\\\":[{\\\"required\\\":true,\\\"message\\\":\\\"请输入系统名称\\\"}],\\\"disabled\\\":false,\\\"readonly\\\":false}\",\"sort\":100,\"title\":\"系统名称\",\"type\":\"input\",\"value\":\"白手起架\"},{\"configId\":4,\"grouping\":\"SYSTEM\",\"isShow\":true,\"length\":4,\"name\":\"times\",\"options\":\"\",\"remark\":\"\",\"rules\":\"{\\\"rules\\\":[{\\\"required\\\":true,\\\"message\\\":\\\"请选择营业时间\\\"}],\\\"disabled\\\":false,\\\"readonly\\\":false}\",\"sort\":99,\"title\":\"营业时间\",\"type\":\"time-range\",\"value\":\"[\\\"08:00\\\",\\\"21:00\\\"]\"},{\"configId\":5,\"grouping\":\"SYSTEM\",\"isShow\":true,\"length\":4,\"name\":\"service_tel\",\"options\":\"\",\"remark\":\"\",\"rules\":\"{\\\"rules\\\":[{\\\"required\\\":true}]}\",\"sort\":98,\"title\":\"客服电话\",\"type\":\"input\",\"value\":\"400-0000-0000\"},{\"configId\":3,\"grouping\":\"SYSTEM\",\"isShow\":true,\"length\":4,\"name\":\"user_register\",\"options\":\"[{\\\"value\\\":\\\"0\\\",\\\"name\\\":\\\"暂停新用户注册\\\"},{\\\"value\\\":\\\"1\\\",\\\"name\\\":\\\"允许新用户注册\\\"}]\",\"remark\":\"\",\"rules\":\"{\\\"rules\\\":[{\\\"required\\\":true}]}\",\"sort\":90,\"title\":\"用户注册\",\"type\":\"select\",\"value\":\"1\"},{\"configId\":2,\"grouping\":\"SYSTEM\",\"isShow\":true,\"length\":12,\"name\":\"copy_right\",\"options\":\"\",\"remark\":\"\",\"rules\":\"{\\\"rules\\\":[]}\",\"sort\":0,\"title\":\"版权信息\",\"type\":\"textarea\",\"value\":\"Prower © 2024 Created by 白手起架\"}]]','127.0.0.1','2024-01-26 15:43:30'),
	(11,'service','管理员','修改角色授权','com.kintreda.service.platform.controller.RoleController.auth','[{\"roleId\":2,\"roles\":[1,2,9,51,3,4,30,40,41,42,17,26,27,52,53,57,58,18,35,36]}]','127.0.0.1','2024-01-26 15:44:45'),
	(12,'service','管理员','添加或修改客户端版本数据','com.kintreda.service.platform.controller.VersionController.save','[{\"description\":\"1.新版本上线\\n2.修复bug\",\"id\":3,\"isForce\":1,\"platform\":\"android\",\"status\":1,\"updateTime\":\"2024-01-26T15:46:05.142\",\"url\":\"https://test.cicms.cn/down/app/v1.2.3.apk\",\"version\":\"v1.0.0\"}]','127.0.0.1','2024-01-26 15:46:05');

/*!40000 ALTER TABLE `sys_log` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_menu`;

CREATE TABLE `sys_menu` (
  `menu_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `parent_id` int(10) NOT NULL COMMENT '上级菜单',
  `icon` varchar(300) DEFAULT NULL COMMENT '图标',
  `path` varchar(120) DEFAULT NULL COMMENT '文件路径',
  `authorize` varchar(120) DEFAULT NULL COMMENT '权限规则',
  `is_menu` tinyint(2) NOT NULL DEFAULT '1' COMMENT '是否为菜单',
  `is_button` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否为按钮',
  `is_show` tinyint(2) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `grouping` varchar(120) NOT NULL DEFAULT 'Admin' COMMENT '分组',
  PRIMARY KEY (`menu_id`),
  KEY `parent_id` (`parent_id`),
  KEY `is_menu` (`is_menu`),
  KEY `is_button` (`is_button`),
  KEY `is_show` (`is_show`),
  KEY `sort` (`sort`),
  KEY `grouping` (`grouping`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='功能菜单';

LOCK TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;

INSERT INTO `sys_menu` (`menu_id`, `name`, `parent_id`, `icon`, `path`, `authorize`, `is_menu`, `is_button`, `is_show`, `sort`, `grouping`)
VALUES
	(1,'后台管理',0,'dashboard','','admin:admin',1,0,1,10,'platform'),
	(2,'数据看板',1,'dashboard','system/main','system:main',1,0,1,0,'platform'),
	(3,'数据管理',0,'folder','','data:index',1,0,1,0,'platform'),
	(4,'用户管理',3,'user','data/user/index','user:index',1,0,1,97,'platform'),
	(5,'系统日志',1,'book','system/logs/index','logs:list',1,0,1,0,'platform'),
	(6,'管理人员',9,'user','system/admin/list','admin:list',1,0,1,0,'platform'),
	(7,'角色管理',9,'usergroup-add','system/role/list','role:list',1,0,1,0,'platform'),
	(9,'系统管理',0,'tool','','system:index',1,0,1,9,'platform'),
	(10,'相关设置',9,'setting','system/config/index','config:index',1,0,1,1,'platform'),
	(11,'权限菜单',9,'lock','system/menu/list','menu:list',1,0,1,0,'platform'),
	(12,'添加菜单',11,'ios-add','','menu:save',0,1,1,0,'platform'),
	(17,'广告管理',9,'file-image','system/banner/index','advertising:list',1,0,1,0,'platform'),
	(18,'相关协议',9,'file-text','system/agree/index','agree:list',1,0,1,0,'platform'),
	(26,'添加',17,'ios-add','','advertising:save',0,1,1,0,'platform'),
	(27,'删除',17,'ios-close','','advertising:del',0,1,0,0,'platform'),
	(28,'添加管理员',6,'plus-circle','','admin:save',0,1,1,0,'platform'),
	(29,'添加角色',7,'ios-add','','role:save',0,1,1,0,'platform'),
	(30,'修改',4,'','','user:add',0,1,0,0,'platform'),
	(34,'删除菜单',11,'','','menu:del',0,1,0,0,'platform'),
	(35,'添加',18,'','','protocol:save',0,1,1,0,'platform'),
	(36,'删除',18,'','','protocol:del',0,1,0,0,'platform'),
	(44,'删除',7,'',NULL,'role:del',0,1,0,0,'platform'),
	(45,'删除',6,'',NULL,'admin:del',0,1,0,0,'platform'),
	(46,'授权',7,'',NULL,'role:auth',0,1,0,0,'platform'),
	(49,'订单',0,'','','order:list',1,0,1,0,'api'),
	(50,'首页',0,'','','home:index',1,0,1,0,'api'),
	(51,'位置管理',17,'','','advertising:position',0,1,1,0,'platform'),
	(52,'添加广告位',17,'','','advertising:position:save',0,1,0,0,'platform'),
	(53,'删除广告位',17,'','','advertising:position:del',0,1,0,0,'platform'),
	(54,'反馈数据',9,'message','system/feedback/index','feedback:list',1,0,1,0,'platform'),
	(55,'回复',54,'','','feedback:replay',0,1,0,0,'platform'),
	(56,'删除',54,'','','feedback:del',0,1,0,0,'platform'),
	(57,'版本管理',9,'bell','system/version/index','version:list',1,0,1,0,'platform'),
	(58,'发布新版本',57,'md-add','','version:save',0,1,1,0,'platform'),
	(59,'删除',57,'','','version:del',0,1,0,0,'platform'),
	(64,'定时任务',9,'history','system/task/index','task:list',1,0,1,0,'platform'),
	(65,'添加新任务',64,'','','task:save',0,1,1,0,'platform'),
	(66,'操作',64,'','','task:active',0,1,0,0,'platform'),
	(67,'删除',64,'','','task:del',0,1,0,0,'platform');

/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_protocol
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_protocol`;

CREATE TABLE `sys_protocol` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `protocol` varchar(120) NOT NULL DEFAULT '' COMMENT '协议标识',
  `title` varchar(180) NOT NULL DEFAULT '' COMMENT '协议名称(标题)',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `content` mediumtext NOT NULL COMMENT '协议内容',
  PRIMARY KEY (`id`),
  KEY `protocol` (`protocol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='协议';

LOCK TABLES `sys_protocol` WRITE;
/*!40000 ALTER TABLE `sys_protocol` DISABLE KEYS */;

INSERT INTO `sys_protocol` (`id`, `protocol`, `title`, `update_time`, `content`)
VALUES
	(1,'USER_PRIVATE_PAOTOCOL','用户隐私协议','2021-01-18 09:35:58','<p>用户隐私协议</p>');

/*!40000 ALTER TABLE `sys_protocol` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `role_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL DEFAULT '0' COMMENT '角色名称',
  `description` varchar(300) DEFAULT NULL COMMENT '描述',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`role_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色信息表';

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;

INSERT INTO `sys_role` (`role_id`, `name`, `description`, `status`)
VALUES
	(1,'超级管理员','超级管理员拥有一切权限',1),
	(2,'普通管理员','非常普通的管理员',1),
	(3,'用户','APP端注册',1);

/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_role_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_role_menu`;

CREATE TABLE `sys_role_menu` (
  `role_id` int(10) NOT NULL COMMENT '角色id',
  `menu_id` int(10) NOT NULL COMMENT '菜单id',
  KEY `role_id` (`role_id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色菜单对应权限表';

LOCK TABLES `sys_role_menu` WRITE;
/*!40000 ALTER TABLE `sys_role_menu` DISABLE KEYS */;

INSERT INTO `sys_role_menu` (`role_id`, `menu_id`)
VALUES
	(1,0),
	(3,49),
	(3,50),
	(2,1),
	(2,2),
	(2,9),
	(2,51),
	(2,3),
	(2,4),
	(2,30),
	(2,40),
	(2,41),
	(2,42),
	(2,17),
	(2,26),
	(2,27),
	(2,52),
	(2,53),
	(2,57),
	(2,58),
	(2,18),
	(2,35),
	(2,36);

/*!40000 ALTER TABLE `sys_role_menu` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_task
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_task`;

CREATE TABLE `sys_task` (
  `task_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bean_name` varchar(80) NOT NULL DEFAULT '' COMMENT 'bean名称',
  `method_name` varchar(120) NOT NULL DEFAULT '' COMMENT '方法名称',
  `method_params` varchar(300) DEFAULT NULL COMMENT '方法参数',
  `cron_expression` varchar(120) NOT NULL DEFAULT '' COMMENT 'cron表达式',
  `remark` varchar(300) DEFAULT NULL COMMENT '备注说明',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态(1=正常,0=暂停)',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sys_task` WRITE;
/*!40000 ALTER TABLE `sys_task` DISABLE KEYS */;

INSERT INTO `sys_task` (`task_id`, `bean_name`, `method_name`, `method_params`, `cron_expression`, `remark`, `status`, `create_time`, `update_time`)
VALUES
	(1,'demoTask','taskNoParams','','0/10 * * * * ?','无参数测试方法',0,'2021-07-09 17:12:08','2022-09-21 09:34:57'),
	(2,'demoTask','taskWithParams','a=1&b=2&abababcucuculsldj=33333','0/3 * * * * ?','带参数测试',0,'2021-07-09 17:13:35','2022-09-21 09:34:47'),
	(3,'demoTask','test','','0/10 * * * * ?','暂停几秒测试',0,'2021-07-09 17:13:23','2022-09-21 09:34:52');

/*!40000 ALTER TABLE `sys_task` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) NOT NULL COMMENT '所属角色',
  `nick_name` varchar(120) NOT NULL DEFAULT '' COMMENT '昵称',
  `phone` varchar(80) DEFAULT NULL COMMENT '手机号',
  `avatar` varchar(300) DEFAULT NULL COMMENT '头像',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`user_id`),
  KEY `role_id` (`role_id`),
  KEY `status` (`status`),
  KEY `phone` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='普通用户';

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;

INSERT INTO `sys_user` (`user_id`, `role_id`, `nick_name`, `phone`, `avatar`, `status`)
VALUES
	(1,3,'test','15348510008',NULL,1);

/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;


# 转储表 sys_version
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_version`;

CREATE TABLE `sys_version` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `platform` varchar(120) NOT NULL DEFAULT '' COMMENT 'android|ios',
  `version` varchar(80) NOT NULL DEFAULT '' COMMENT '版本号',
  `description` mediumtext COMMENT '版本说明',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `is_force` int(11) NOT NULL DEFAULT '0' COMMENT '是否强制更新',
  `url` varchar(300) NOT NULL DEFAULT '' COMMENT '更新包下载地址',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '是否发布',
  PRIMARY KEY (`id`),
  KEY `platform` (`platform`),
  KEY `version` (`version`),
  KEY `update_time` (`update_time`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='APP版本管理';

LOCK TABLES `sys_version` WRITE;
/*!40000 ALTER TABLE `sys_version` DISABLE KEYS */;

INSERT INTO `sys_version` (`id`, `platform`, `version`, `description`, `update_time`, `is_force`, `url`, `status`)
VALUES
	(3,'android','v1.0.0','1.新版本上线\n2.修复bug','2024-01-26 15:46:05',1,'https://test.cicms.cn/down/app/v1.2.3.apk',1);

/*!40000 ALTER TABLE `sys_version` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
