package com.kintreda.common.mybatis.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.mybatis.entity.SysFeedback;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kintreda.common.mybatis.model.FeedbackModel;

/**
 * <p>
 * 反馈数据 Mapper 接口
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
public interface SysFeedbackMapper extends BaseMapper<SysFeedback> {

    /**
     * 反馈内容管理
     * @param page
     * @param param
     * @return
     */
    IPage<SysFeedback> managePage(IPage<SysFeedback> page, FeedbackModel param);
}
