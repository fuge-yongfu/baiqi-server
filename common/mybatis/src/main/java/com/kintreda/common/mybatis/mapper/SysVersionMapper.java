package com.kintreda.common.mybatis.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.mybatis.entity.SysVersion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kintreda.common.mybatis.model.VersionModel;

/**
 * <p>
 * APP版本管理 Mapper 接口
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
public interface SysVersionMapper extends BaseMapper<SysVersion> {

    /**
     * 版本管理
     * @param page
     * @param param
     * @return
     */
    IPage<SysVersion> managerPage(IPage<SysVersion> page, VersionModel param);
}
