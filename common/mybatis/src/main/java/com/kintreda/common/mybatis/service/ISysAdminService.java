package com.kintreda.common.mybatis.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.mybatis.entity.SysAdmin;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kintreda.common.mybatis.model.BasePageModel;

/**
 * <p>
 * 管理人员 服务类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-09
 */
public interface ISysAdminService extends IService<SysAdmin> {

    SysAdmin login(String username, String password);

    IPage<SysAdmin> listByManager(BasePageModel param);
}
