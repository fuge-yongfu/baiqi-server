package com.kintreda.common.mybatis;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MybatisPlus {

    public static void main(String[] args) {


        //过滤表前缀
        List<String> prefix = Collections.singletonList("default_");
        //生成的表名
        List<String> include = Collections.singletonList("sys_config");




        FastAutoGenerator fastAutoGenerator = FastAutoGenerator.create("jdbc:mysql://192.168.2.10:3306/baishou?useUnicode=true&characterEncoding=utf8", "root", "7fbf2c4ac7a1ec93");
        fastAutoGenerator.globalConfig(builder -> {
                    builder.author("Yongfu") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .disableOpenDir()
                            .outputDir(System.getProperty("user.dir") + "/common/mybatis/src/main/java"); // 指定输出目录
                }).packageConfig(builder -> {
                    builder.parent("com.kintreda.common.mybatis");
                }).strategyConfig(builder -> {
                    builder.addTablePrefix(prefix)// 设置过滤表前缀
                            .addInclude(include) // 设置需要生成的表名
                            .entityBuilder()
                            .enableLombok();
                }).templateConfig(builder -> {
                    builder.controller(null);//不生成controller
                }).templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }

}
