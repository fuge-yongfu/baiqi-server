package com.kintreda.common.mybatis.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kintreda.common.mybatis.entity.SysProtocol;
import com.kintreda.common.mybatis.mapper.SysProtocolMapper;
import com.kintreda.common.mybatis.model.BasePageModel;
import com.kintreda.common.mybatis.service.ISysProtocolService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 协议 服务实现类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
@Service
public class SysProtocolServiceImpl extends ServiceImpl<SysProtocolMapper, SysProtocol> implements ISysProtocolService {

    /**
     * 协议管理列表
     * @param pageModel
     * @return
     */
    @Override
    public IPage<SysProtocol> managerList(BasePageModel pageModel) {
        IPage<SysProtocol> page = new Page<>(pageModel.getPage(),pageModel.getLimit());
        QueryWrapper<SysProtocol> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(pageModel.getKeyword())){
            queryWrapper.like("title",pageModel.getKeyword());
            queryWrapper.or();
            queryWrapper.like("protocol",pageModel.getKeyword());
        }
        return page(page,queryWrapper);
    }


}
