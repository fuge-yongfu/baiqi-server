package com.kintreda.common.mybatis.service;

import com.kintreda.common.mybatis.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kintreda.common.mybatis.vos.ManagerMenuVo;
import com.kintreda.common.mybatis.vos.TreeMenuVo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 功能菜单 服务类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-12
 */
public interface ISysMenuService extends IService<SysMenu> {

    List<SysMenu> getChildrenList(int parentId, Collection<Integer> menuIds, String group);

    List<ManagerMenuVo> getChildrenListByManager(int parentId, Collection<Integer> menuIds, String group);

    List<SysMenu> getAuthorizeByMenuIds(Collection<Integer> roleIds,String group);

    List<SysMenu> getAllMenu(String group);

    List<ManagerMenuVo> getTreeList(Integer parentId, String group, Collection<Integer> menuIds);

    SysMenu getAuthorizeByUrl(String uri, String group);
}
