package com.kintreda.common.mybatis.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 反馈数据
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_feedback")
@ApiModel(value="SysFeedback对象", description="反馈数据")
public class SysFeedback implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "姓名")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "手机号")
    @TableField("phone")
    private String phone;

    @ApiModelProperty(value = "反馈内容")
    @TableField("content")
    private String content;

    @ApiModelProperty(value = "反馈时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "回复内容")
    @TableField("replay")
    private String replay;

    @ApiModelProperty(value = "验证码")
    @TableField(exist = false)
    private String token;

}
