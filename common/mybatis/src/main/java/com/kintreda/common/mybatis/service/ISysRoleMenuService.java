package com.kintreda.common.mybatis.service;

import com.kintreda.common.mybatis.entity.SysMenu;
import com.kintreda.common.mybatis.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kintreda.common.mybatis.vos.TreeMenuVo;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色菜单对应权限表 服务类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-09
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

    List<TreeMenuVo> menuTreeByRoleId(Integer roleId, String group);

    List<String> getRolesByRoleId(Integer roleId, String group);

    Collection<String> getRoles(Integer menuId);

    Boolean removeByRoleId(Integer roleId);

    Collection<Integer> getMenuIdsByRoleId(Integer roleId, String group);

    List<Map<String, Object>> getRolesListByRoleId(Integer roleId, String group);
}
