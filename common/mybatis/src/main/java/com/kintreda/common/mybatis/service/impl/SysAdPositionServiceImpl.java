package com.kintreda.common.mybatis.service.impl;

import com.kintreda.common.mybatis.entity.SysAdPosition;
import com.kintreda.common.mybatis.mapper.SysAdPositionMapper;
import com.kintreda.common.mybatis.service.ISysAdPositionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 广告位置 服务实现类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
@Service
public class SysAdPositionServiceImpl extends ServiceImpl<SysAdPositionMapper, SysAdPosition> implements ISysAdPositionService {

    /**
     * 获取所有的广告位置
     * @return
     */
    @Override
    public List<SysAdPosition> getAll() {
        return list();
    }
}
