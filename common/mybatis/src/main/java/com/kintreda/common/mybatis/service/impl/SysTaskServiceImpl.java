package com.kintreda.common.mybatis.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kintreda.common.mybatis.entity.SysTask;
import com.kintreda.common.mybatis.mapper.SysTaskMapper;
import com.kintreda.common.mybatis.model.BasePageModel;
import com.kintreda.common.mybatis.service.ISysTaskService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Yongfu
 * @since 2021-07-09
 */
@Service
public class SysTaskServiceImpl extends ServiceImpl<SysTaskMapper, SysTask> implements ISysTaskService {

    @Override
    public IPage<SysTask> getList(BasePageModel param) {
        IPage<SysTask> page = new Page<>(param.getPage(),param.getLimit());
        return baseMapper.getList(page,param);
    }
}
