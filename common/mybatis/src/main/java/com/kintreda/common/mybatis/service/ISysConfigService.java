package com.kintreda.common.mybatis.service;

import com.kintreda.common.config.R;
import com.kintreda.common.mybatis.entity.SysConfig;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kintreda.common.mybatis.enums.ConfigGroupEnum;
import com.kintreda.common.mybatis.model.ConfigGroupModel;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 配置信息 服务类
 * </p>
 *
 * @author Yongfu
 * @since 2024-01-26
 */
public interface ISysConfigService extends IService<SysConfig> {

    String getValueByName(String name);

    Map<String, Object> getListByGrouping(ConfigGroupEnum grouping);

    R<List<ConfigGroupModel>> allConfig();

    void createCache();

}
