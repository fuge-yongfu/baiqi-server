package com.kintreda.common.mybatis.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 广告
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_advertising")
@ApiModel(value="SysAdvertising对象", description="广告")
public class SysAdvertising implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "所属位置",required = true)
    @NotNull(message = "请选择广告位置")
    @TableField("position_id")
    private Integer positionId;

    @ApiModelProperty(value = "广告名称",required = true)
    @NotBlank(message = "请填写广告名称")
    @TableField("title")
    private String title;

    @ApiModelProperty(value = "图片地址",required = true)
    @NotBlank(message = "请上传广告图片")
    @TableField("src")
    private String src;

    @ApiModelProperty(value = "跳转地址")
    @TableField("link")
    private String link;

    @ApiModelProperty(value = "排序")
    @TableField("sort")
    private Integer sort;

    @ApiModelProperty(value = "状态")
    @TableField("status")
    private Integer status;

    // TODO: 2021/1/18 自定义字段
    @ApiModelProperty(value = "位置名称")
    @TableField(exist = false)
    private String positionName;


}
