package com.kintreda.common.mybatis.service;

import com.kintreda.common.mybatis.entity.SysLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 操作日志 服务类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-13
 */
public interface ISysLogService extends IService<SysLog> {

}
