package com.kintreda.common.mybatis.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("用户管理查询参数")
public class UserPageModel extends BasePageModel{

    @ApiModelProperty(value = "角色Id")
    private Integer roleId;


}
