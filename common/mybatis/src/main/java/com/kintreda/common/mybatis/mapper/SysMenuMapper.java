package com.kintreda.common.mybatis.mapper;

import com.kintreda.common.mybatis.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kintreda.common.mybatis.vos.ManagerMenuVo;
import com.kintreda.common.mybatis.vos.TreeMenuVo;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 功能菜单 Mapper 接口
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-12
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 获取指定节点下包含menuIds的菜单
     * @param parentId
     * @param menuIds
     * @param group
     * @return
     */
    List<SysMenu> getChildrenList(int parentId, Collection<Integer> menuIds, String group);

    /**
     * 获取权限列表
     * @param menuIds
     * @param group
     * @return
     */
    List<SysMenu> getAuthorizeByMenuIds(Collection<Integer> menuIds, String group);

    /**
     * 管理菜单
     * @param parentId
     * @param menuIds
     * @param group
     * @return
     */
    List<ManagerMenuVo> getChildrenListByManager(int parentId, Collection<Integer> menuIds, String group);
}
