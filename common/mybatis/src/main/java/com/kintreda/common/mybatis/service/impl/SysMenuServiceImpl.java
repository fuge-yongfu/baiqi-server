package com.kintreda.common.mybatis.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kintreda.common.mybatis.entity.SysMenu;
import com.kintreda.common.mybatis.mapper.SysMenuMapper;
import com.kintreda.common.mybatis.service.ISysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kintreda.common.mybatis.vos.ManagerMenuVo;
import com.kintreda.common.mybatis.vos.TreeMenuVo;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 功能菜单 服务实现类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-12
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {

    /**
     * 获取指定节点下包含menuIds的菜单
     * @param parentId
     * @param menuIds
     * @param group
     * @return
     */
    @Override
    public List<SysMenu> getChildrenList(int parentId, Collection<Integer> menuIds, String group) {
        return baseMapper.getChildrenList(parentId,menuIds,group);
    }

    /**
     * 管理菜单
     * @param parentId
     * @param menuIds
     * @param group
     * @return
     */
    @Override
    public List<ManagerMenuVo> getChildrenListByManager(int parentId, Collection<Integer> menuIds, String group) {
        return baseMapper.getChildrenListByManager(parentId,menuIds,group);
    }

    /**
     * 获取权限列表
     * @param menuIds
     * @param group
     * @return
     */
    @Override
    public List<SysMenu> getAuthorizeByMenuIds(Collection<Integer> menuIds, String group) {
        return baseMapper.getAuthorizeByMenuIds(menuIds,group);
    }

    /**
     * 获取所有的权限列表
     * @return
     */
    @Override
    public List<SysMenu> getAllMenu(String group) {
        QueryWrapper<SysMenu> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(group) && !"".equals(group)){
            queryWrapper.eq("grouping",group);
        }
        return list(queryWrapper);
    }

    /**
     * 获取树形菜单
     * @param parentId
     * @param group
     * @param menuIds
     * @return
     */
    @Override
    public List<ManagerMenuVo> getTreeList(Integer parentId, String group,  Collection<Integer> menuIds) {
        List<ManagerMenuVo> childrenListByManager = getChildrenListByManager(parentId, menuIds, group);
        if (ObjectUtils.isNotEmpty(childrenListByManager)){
            childrenListByManager.forEach(managerMenuVo -> {
                List<ManagerMenuVo> treeList = this.getTreeList(managerMenuVo.getMenuId(), group, menuIds);
                managerMenuVo.setChildren(treeList);
                //默认展开一节节点
                if (managerMenuVo.getParentId()==0){
                    managerMenuVo.set_showChildren(true);
                }
                //checked
                if (menuIds.size()>0 && menuIds.contains(managerMenuVo.getMenuId())){
                    managerMenuVo.setChecked(true);
                }
                //
                managerMenuVo.setLabel(managerMenuVo.getName());
                managerMenuVo.setTitle(managerMenuVo.getName());
                managerMenuVo.setValue(managerMenuVo.getMenuId());
            });
        }
        return childrenListByManager;
    }

    /**
     * 匹配访问路径的权限
     * @param uri
     * @param group
     * @return
     */
    @Override
    public SysMenu getAuthorizeByUrl(String uri, String group) {
        QueryWrapper<SysMenu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("authorize",uri);
        if (StringUtils.isNotBlank(group)){
            queryWrapper.eq("grouping",group);
        }
        List<SysMenu> list = this.list(queryWrapper);
        if (ObjectUtils.isNotEmpty(list) && list.size()>0){
            SysMenu res = list.get(0);
            int length = res.getAuthorize().length();
            for (SysMenu sysMenu : list) {
                if(sysMenu.getAuthorize().length()<length){
                    res = sysMenu;
                    length = sysMenu.getAuthorize().length();
                }
            }
            return res;
        }else{
            return null;
        }
    }


}
