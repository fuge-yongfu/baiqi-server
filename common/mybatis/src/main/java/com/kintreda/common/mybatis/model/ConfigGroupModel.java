package com.kintreda.common.mybatis.model;

import com.kintreda.common.mybatis.entity.SysConfig;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("配置分组")
public class ConfigGroupModel {
    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "code")
    private String code;

    @ApiModelProperty(value = "配置")
    private List<SysConfig> configs;

}
