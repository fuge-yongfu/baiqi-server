package com.kintreda.common.mybatis.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kintreda.common.mybatis.entity.SysFeedback;
import com.kintreda.common.mybatis.mapper.SysFeedbackMapper;
import com.kintreda.common.mybatis.model.FeedbackModel;
import com.kintreda.common.mybatis.service.ISysFeedbackService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 反馈数据 服务实现类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
@Service
public class SysFeedbackServiceImpl extends ServiceImpl<SysFeedbackMapper, SysFeedback> implements ISysFeedbackService {

    /**
     * 反馈内容管理
     * @param param
     * @return
     */
    @Override
    public IPage<SysFeedback> managerPage(FeedbackModel param) {
        IPage<SysFeedback> page = new Page<>(param.getPage(),param.getLimit());
        IPage<SysFeedback> result = baseMapper.managePage(page,param);
        return result;
    }
}
