package com.kintreda.common.mybatis.vos;

import com.kintreda.common.mybatis.entity.SysMenu;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("功能菜单树形菜单")
public class TreeMenuVo extends SysMenu {

    @ApiModelProperty(value = "子节点")
    private List<TreeMenuVo> children = new ArrayList<>();

    @ApiModelProperty(value = "文件路径")
    private String url = "";

    @ApiModelProperty(value = "菜单数量")
    private Integer menuNum = 0;

    @ApiModelProperty(value = "按钮数量")
    private Integer btnNum = 0;

//    @ApiModelProperty(value = "按钮列表")
//    private List<TreeMenuVo> buttons = new ArrayList<>();


}
