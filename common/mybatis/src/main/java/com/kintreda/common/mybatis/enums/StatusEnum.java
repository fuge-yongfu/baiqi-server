package com.kintreda.common.mybatis.enums;


/**
 * 通用状态枚举
 */
public enum StatusEnum {
    关闭(0,"禁用"),
    开启(1,"启用");

    private Integer status;
    private String name;

    StatusEnum(Integer status,String name){
        this.status = status;
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
