package com.kintreda.common.mybatis.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.mybatis.entity.SysAdmin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kintreda.common.mybatis.model.BasePageModel;

/**
 * <p>
 * 管理人员 Mapper 接口
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-09
 */
public interface SysAdminMapper extends BaseMapper<SysAdmin> {

    /**
     * 管理员列表
     * @param page
     * @param param
     * @return
     */
    IPage<SysAdmin> listByManager(IPage<SysAdmin> page, BasePageModel param);
}
