package com.kintreda.common.mybatis.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.mybatis.entity.SysFeedback;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kintreda.common.mybatis.model.FeedbackModel;

/**
 * <p>
 * 反馈数据 服务类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
public interface ISysFeedbackService extends IService<SysFeedback> {

    IPage<SysFeedback> managerPage(FeedbackModel param);
}
