package com.kintreda.common.mybatis.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kintreda.common.mybatis.entity.SysMenu;
import com.kintreda.common.mybatis.entity.SysRoleMenu;
import com.kintreda.common.mybatis.mapper.SysRoleMenuMapper;
import com.kintreda.common.mybatis.service.ISysMenuService;
import com.kintreda.common.mybatis.service.ISysRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kintreda.common.mybatis.vos.TreeMenuVo;
import org.apache.commons.collections4.QueueUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * 角色菜单对应权限表 服务实现类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-09
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {

    @Autowired
    private ISysMenuService iSysMenuService;

    /**
     * 获取某个角色的菜单列表
     * @param roleId 角色ID
     * @param group 平台
     * @return
     */
    @Override
    public List<TreeMenuVo> menuTreeByRoleId(Integer roleId, String group) {
        //有权限的菜单id
        Collection<Integer> roleIds = getMenuIdsByRoleId(roleId,group);
        return this.getMenuTreeAll(0,roleIds,group);
    }

    /**
     * 获取权限列表
     * @param roleId
     * @param group
     * @return
     */
    @Override
    public List<String> getRolesByRoleId(Integer roleId, String group) {
        //有权限的菜单id
        Collection<Integer> roleIds = getMenuIdsByRoleId(roleId,group);
        //获取权限列表
        List<SysMenu> menuList = new ArrayList<>();
        if (roleIds.size()>0 || roleId==1){
            menuList = iSysMenuService.getAuthorizeByMenuIds(roleIds,group);
        }
        //权限集合
        List<String> roleVos = new ArrayList<>();
        if (ObjectUtils.isNotEmpty(menuList)){
            menuList.forEach(sysMenu -> {
                roleVos.add(sysMenu.getAuthorize());
            });
        }
        return roleVos;
    }

    /**
     * 获取某个菜单节点哪些角色可以访问
     * @param menuId
     * @return
     */
    @Override
    public Collection<String> getRoles(Integer menuId) {
        QueryWrapper<SysRoleMenu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("menu_id",menuId);
        List<SysRoleMenu> list = list(queryWrapper);
        Collection<String> roles = new ArrayList<>();
        if (ObjectUtils.isNotEmpty(list) && list.size()>0){
            list.forEach(x->{
                roles.add(x.getRoleId().toString());
            });
        }
        return roles;
    }

    /**
     * 删除某个角色的所有权限
     * @param roleId
     * @return
     */
    @Override
    public Boolean removeByRoleId(Integer roleId) {
        QueryWrapper<SysRoleMenu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("role_id",roleId);
        if (count(queryWrapper)<1){
            return true;
        }
        return remove(queryWrapper);
    }

    /**
     * 获取某个角色的已经拥有的菜单Id
     * @param roleId
     * @param group
     * @return
     */
    @Override
    public Collection<Integer> getMenuIdsByRoleId(Integer roleId, String group) {
        //有权限的菜单id
        Collection<Integer> roleIds = new ArrayList<>();
        if (roleId>1){
            //获取所有可用的菜单
            QueryWrapper<SysRoleMenu> sysRoleMenuQueryWrapper = new QueryWrapper<>();
            sysRoleMenuQueryWrapper.eq("role_id",roleId);
            List<SysRoleMenu> roleMenuList = this.list(sysRoleMenuQueryWrapper);
            if (ObjectUtils.isEmpty(roleMenuList)){
                return new ArrayList<>();//没有权限
            }
            roleMenuList.forEach(sysRoleMenu -> {
                roleIds.add(sysRoleMenu.getMenuId());
            });
        }
        return roleIds;
    }

    /**
     * 获取权限菜单列表
     * @param roleId
     * @param group
     * @return
     */
    @Override
    public List<Map<String, Object>> getRolesListByRoleId(Integer roleId, String group) {
        //有权限的菜单id
        Collection<Integer> roleIds = getMenuIdsByRoleId(roleId,group);
        if (roleId>1 && roleIds.size()<1){
            return new ArrayList<>();
        }
        //获取权限列表
        List<SysMenu> menuList = iSysMenuService.getAuthorizeByMenuIds(roleIds,group);
        //权限集合
        List<Map<String,Object>> roleVos = new ArrayList<>();
        if (ObjectUtils.isNotEmpty(menuList)){
            menuList.forEach(sysMenu -> {
                Map<String,Object> vo = new HashMap<>();
                vo.put("authorize",sysMenu.getAuthorize());
                vo.put("name",sysMenu.getName());
                roleVos.add(vo);
            });
        }
        return roleVos;
    }

    /**
     * 获取树形菜单
     * @param parentId
     * @param roleIds
     * @param group
     * @return
     */
    private List<TreeMenuVo> getMenuTreeAll(int parentId, Collection<Integer> roleIds, String group) {
        List<TreeMenuVo> treeMenuVos = new ArrayList<>();
        List<SysMenu> childrenList = iSysMenuService.getChildrenList(parentId, roleIds, group);
        if (ObjectUtils.isEmpty(childrenList) || childrenList.size()<1){
            return treeMenuVos;
        }
        childrenList.forEach(sysMenu -> {
            TreeMenuVo treeMenuVo = new TreeMenuVo();
            treeMenuVo.setAuthorize(sysMenu.getAuthorize());
            treeMenuVo.setGrouping(sysMenu.getGrouping());
            treeMenuVo.setIcon(sysMenu.getIcon());
            treeMenuVo.setMenuId(sysMenu.getMenuId());
            treeMenuVo.setIsButton(sysMenu.getIsButton());
            treeMenuVo.setIsShow(sysMenu.getIsShow());
            treeMenuVo.setName(sysMenu.getName());
            treeMenuVo.setParentId(sysMenu.getParentId());
            treeMenuVo.setIsMenu(sysMenu.getIsMenu());
            treeMenuVo.setSort(sysMenu.getSort());
            treeMenuVo.setChildren(getMenuTreeAll(sysMenu.getMenuId(),roleIds,group));
            //转换路径
            treeMenuVo.setUrl(sysMenu.getPath());
            //计算按钮数量
            if(treeMenuVo.getChildren().size()>0){
                treeMenuVo.getChildren().forEach(children->{
                    if (children.getIsButton()>0){
                        treeMenuVo.setBtnNum(treeMenuVo.getBtnNum() +1);
                    }
                    if (children.getIsMenu()>0){
                        treeMenuVo.setMenuNum(treeMenuVo.getMenuNum() +1);
                    }
                });
            }
            treeMenuVos.add(treeMenuVo);
        });
        return treeMenuVos;
    }


}
