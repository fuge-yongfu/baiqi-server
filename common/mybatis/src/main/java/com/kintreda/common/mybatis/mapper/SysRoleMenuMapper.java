package com.kintreda.common.mybatis.mapper;

import com.kintreda.common.mybatis.entity.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色菜单对应权限表 Mapper 接口
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-09
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
