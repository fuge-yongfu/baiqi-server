package com.kintreda.common.mybatis.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 配置分组
 */
@Getter
public enum ConfigGroupEnum {
    系统配置("SYSTEM","系统配置"),
    其他配置("OTHER","其他配置"),
    ;
    @JsonValue
    private String name;
    @EnumValue
    private String group;

    ConfigGroupEnum(String group, String name){
        this.name = name;
        this.group = group;
    }

    /**
     * 获取所有的配置分类
     * @return
     */
    public static List<Map<String,String>> all(){
        List<Map<String,String>> result = new ArrayList<>();
        for (ConfigGroupEnum item : ConfigGroupEnum.values()) {
            Map<String,String> data = new HashMap<>();
            data.put("name",item.getName());
            data.put("group",item.getGroup());
            result.add(data);
        }
        return result;
    }

}
