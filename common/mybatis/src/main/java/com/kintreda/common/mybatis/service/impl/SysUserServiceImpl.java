package com.kintreda.common.mybatis.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kintreda.common.mybatis.entity.SysUser;
import com.kintreda.common.mybatis.mapper.SysUserMapper;
import com.kintreda.common.mybatis.model.UserPageModel;
import com.kintreda.common.mybatis.service.ISysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 普通用户 服务实现类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-16
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    /**
     * 手机号登录
     * @param phone
     * @return
     */
    @Override
    public SysUser loginByPhone(String phone) {
        QueryWrapper<SysUser> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq("phone",phone);
        return getOne(userQueryWrapper);
    }

    /**
     * 用户管理
     * @param param
     * @return
     */
    @Override
    public IPage<SysUser> listByManager(UserPageModel param) {
        IPage<SysUser> page = new Page<>(param.getPage(),param.getLimit());
        return baseMapper.listByManager(page,param);
    }

    /**
     * 根据用户Id获取用户信息
     * @param userId
     * @return
     */
    @Override
    public SysUser getByUserId(String userId) {
        return baseMapper.getByUserId(userId);
    }

    /**
     * 获取微信小程序用户
     * @param openId
     * @return
     */
    @Override
    public SysUser getByWxmpOpenId(String openId) {
        return baseMapper.getByWxmpOpenId(openId);
    }


}
