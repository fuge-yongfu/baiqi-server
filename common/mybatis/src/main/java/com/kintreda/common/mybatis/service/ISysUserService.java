package com.kintreda.common.mybatis.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.mybatis.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kintreda.common.mybatis.model.UserPageModel;

/**
 * <p>
 * 普通用户 服务类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-16
 */
public interface ISysUserService extends IService<SysUser> {

    SysUser loginByPhone(String phone);

    IPage<SysUser> listByManager(UserPageModel param);

    SysUser getByUserId(String userId);

    SysUser getByWxmpOpenId(String openId);
}
