package com.kintreda.common.mybatis.vos;

import com.kintreda.common.mybatis.entity.SysMenu;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "菜单管理")
public class ManagerMenuVo extends SysMenu {

    @ApiModelProperty(value = "label")
    private String label;

    @ApiModelProperty(value = "title")
    private String title;

    @ApiModelProperty(value = "value")
    private Integer value;

    @ApiModelProperty(value = "children")
    private List<ManagerMenuVo> children = new ArrayList<>();

    @ApiModelProperty(value = "checked")
    private Boolean checked = false;

    @ApiModelProperty(value = "是否默认展开")
    private Boolean _showChildren = false;

}
