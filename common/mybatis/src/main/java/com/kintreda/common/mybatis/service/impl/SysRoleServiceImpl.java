package com.kintreda.common.mybatis.service.impl;

import com.kintreda.common.mybatis.entity.SysRole;
import com.kintreda.common.mybatis.mapper.SysRoleMapper;
import com.kintreda.common.mybatis.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色信息表 服务实现类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-09
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

}
