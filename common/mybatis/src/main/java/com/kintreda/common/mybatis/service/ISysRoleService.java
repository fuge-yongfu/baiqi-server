package com.kintreda.common.mybatis.service;

import com.kintreda.common.mybatis.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色信息表 服务类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-09
 */
public interface ISysRoleService extends IService<SysRole> {

}
