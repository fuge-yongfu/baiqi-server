package com.kintreda.common.mybatis.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kintreda.common.mybatis.entity.SysAdvertising;
import com.kintreda.common.mybatis.mapper.SysAdvertisingMapper;
import com.kintreda.common.mybatis.model.AdvertisingModel;
import com.kintreda.common.mybatis.service.ISysAdvertisingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 广告 服务实现类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
@Service
public class SysAdvertisingServiceImpl extends ServiceImpl<SysAdvertisingMapper, SysAdvertising> implements ISysAdvertisingService {

    /**
     * 广告管理
     * @param param
     * @return
     */
    @Override
    public IPage<SysAdvertising> managerPage(AdvertisingModel param) {
        IPage<SysAdvertising> page = new Page<>(param.getPage(),param.getLimit());
        IPage<SysAdvertising> result = baseMapper.managerPage(page,param);
        return result;
    }
}
