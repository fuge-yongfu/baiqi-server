package com.kintreda.common.mybatis.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * APP版本管理
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_version")
@ApiModel(value="SysVersion对象", description="APP版本管理")
public class SysVersion implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "android|ios",required = true)
    @NotBlank(message = "请选择平台")
    @TableField("platform")
    private String platform;

    @ApiModelProperty(value = "版本号",required = true)
    @NotBlank(message = "请填写版本号")
    @TableField("version")
    private String version;

    @ApiModelProperty(value = "版本说明")
    @TableField("description")
    private String description;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_time")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "是否强制更新")
    @TableField("is_force")
    private Integer isForce;

    @ApiModelProperty(value = "更新包下载地址",required = true)
    @NotBlank(message = "请填写更新包下载地址")
    @TableField("url")
    private String url;

    @ApiModelProperty(value = "是否发布")
    @TableField("status")
    private Integer status;


}
