package com.kintreda.common.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 配置信息
 * </p>
 *
 * @author Yongfu
 * @since 2024-01-26
 */
@Getter
@Setter
@TableName("sys_config")
@ApiModel(value = "SysConfig对象", description = "配置信息")
public class SysConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "config_id", type = IdType.AUTO)
    private Integer configId;

    @ApiModelProperty("名称")
    private String title;

    @ApiModelProperty("参数名称")
    private String name;

    @ApiModelProperty("参数值")
    private String value;

    @ApiModelProperty("分组")
    private String grouping;

    @ApiModelProperty("设置形式")
    private String type;

    @ApiModelProperty("配置长度")
    private Integer length;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("是否显示")
    private Boolean isShow;

    @ApiModelProperty("参数说明")
    private String remark;

    @ApiModelProperty(value = "验证规则")
    private String rules;

    @ApiModelProperty(value = "选项")
    private String options;


}
