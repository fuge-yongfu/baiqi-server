package com.kintreda.common.mybatis.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.sun.org.apache.xpath.internal.operations.Bool;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 功能菜单
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_menu")
@ApiModel(value="SysMenu对象", description="功能菜单")
public class SysMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "menu_id", type = IdType.AUTO)
    private Integer menuId;

    @ApiModelProperty(value = "菜单名称",required = true)
    @NotBlank(message = "请填写菜单名称")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "上级菜单")
    @TableField("parent_id")
    private Integer parentId = 0;

    @ApiModelProperty(value = "图标")
    @TableField("icon")
    private String icon;

    @ApiModelProperty(value = "权限规则")
    @TableField("authorize")
    private String authorize;

    @ApiModelProperty(value = "组件路径")
    @TableField("path")
    private String path;

    @ApiModelProperty(value = "是否为菜单")
    @TableField("is_menu")
    private Integer isMenu;

    @ApiModelProperty(value = "是否为按钮")
    @TableField("is_button")
    private Integer isButton;

    @ApiModelProperty(value = "是否显示")
    @TableField("is_show")
    private Integer isShow;

    @ApiModelProperty(value = "排序")
    @TableField("sort")
    private Integer sort;

    @ApiModelProperty(value = "分组",required = true)
    @NotBlank(message = "请选择所属单位")
    @TableField("grouping")
    private String grouping;



}
