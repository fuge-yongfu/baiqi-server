package com.kintreda.common.mybatis.mapper;

import com.kintreda.common.mybatis.entity.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 操作日志 Mapper 接口
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-13
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
