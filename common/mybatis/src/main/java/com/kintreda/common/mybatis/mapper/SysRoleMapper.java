package com.kintreda.common.mybatis.mapper;

import com.kintreda.common.mybatis.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色信息表 Mapper 接口
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-09
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
