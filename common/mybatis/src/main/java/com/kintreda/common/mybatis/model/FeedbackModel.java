package com.kintreda.common.mybatis.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("反馈数据参数")
public class FeedbackModel extends BasePageModel {

    @ApiModelProperty(value = "是否回复")
    private Integer isReplay;


}
