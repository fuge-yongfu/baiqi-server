package com.kintreda.common.mybatis.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.mybatis.entity.SysProtocol;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kintreda.common.mybatis.model.BasePageModel;

/**
 * <p>
 * 协议 服务类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
public interface ISysProtocolService extends IService<SysProtocol> {

    IPage<SysProtocol> managerList(BasePageModel pageModel);

}
