package com.kintreda.common.mybatis.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("广告列表筛选参数")
public class AdvertisingModel extends BasePageModel {

    @ApiModelProperty(value = "位置Id")
    private Integer positionId;

    @ApiModelProperty(value = "状态")
    private Integer status;

}
