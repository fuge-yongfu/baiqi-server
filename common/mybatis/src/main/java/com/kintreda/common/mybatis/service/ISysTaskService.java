package com.kintreda.common.mybatis.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.mybatis.entity.SysTask;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kintreda.common.mybatis.model.BasePageModel;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Yongfu
 * @since 2021-07-09
 */
public interface ISysTaskService extends IService<SysTask> {

    IPage<SysTask> getList(BasePageModel param);
}
