package com.kintreda.common.mybatis.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kintreda.common.mybatis.entity.SysAdmin;
import com.kintreda.common.mybatis.mapper.SysAdminMapper;
import com.kintreda.common.mybatis.model.BasePageModel;
import com.kintreda.common.mybatis.service.ISysAdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理人员 服务实现类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-09
 */
@Service
public class SysAdminServiceImpl extends ServiceImpl<SysAdminMapper, SysAdmin> implements ISysAdminService {

    /**
     * 登录
     * @param username
     * @param password
     * @return
     */
    @Override
    public SysAdmin login(String username, String password) {
        QueryWrapper<SysAdmin> sysAdminQueryWrapper = new QueryWrapper<>();
        sysAdminQueryWrapper.eq("username",username);
        return this.getOne(sysAdminQueryWrapper);
    }

    /**
     * 管理员列表
     * @param param
     * @return
     */
    @Override
    public IPage<SysAdmin> listByManager(BasePageModel param) {
        IPage<SysAdmin> page = new Page<>(param.getPage(),param.getLimit());
        IPage<SysAdmin> result = baseMapper.listByManager(page,param);
        return result;
    }


}
