package com.kintreda.common.mybatis.service.impl;

import com.kintreda.common.mybatis.entity.SysLog;
import com.kintreda.common.mybatis.mapper.SysLogMapper;
import com.kintreda.common.mybatis.service.ISysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志 服务实现类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-13
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements ISysLogService {

}
