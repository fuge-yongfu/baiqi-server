package com.kintreda.common.mybatis.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 广告位置
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_ad_position")
@ApiModel(value="SysAdPosition对象", description="广告位置")
public class SysAdPosition implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "位置名称",required = true)
    @NotBlank(message = "请输入位置名称")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "获取数量",required = true)
    @NotNull(message = "请填写调用数量")
    @TableField("num")
    private Integer num;

    @ApiModelProperty(value = "图片宽度")
    @TableField("width")
    private Integer width;

    @ApiModelProperty(value = "图片高度")
    @TableField("height")
    private Integer height;


}
