package com.kintreda.common.mybatis.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("版本管理查询条件")
public class VersionModel extends BasePageModel{

    @ApiModelProperty(value = "平台")
    private String platform;


}
