package com.kintreda.common.mybatis.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.mybatis.entity.SysAdvertising;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kintreda.common.mybatis.model.AdvertisingModel;
import com.kintreda.common.mybatis.model.BasePageModel;

/**
 * <p>
 * 广告 Mapper 接口
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
public interface SysAdvertisingMapper extends BaseMapper<SysAdvertising> {

    /**
     * 广告管理
     * @param page
     * @param param
     * @return
     */
    IPage<SysAdvertising> managerPage(IPage<SysAdvertising> page, AdvertisingModel param);
}
