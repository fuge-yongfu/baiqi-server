package com.kintreda.common.mybatis.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.mybatis.entity.SysVersion;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kintreda.common.mybatis.model.VersionModel;

/**
 * <p>
 * APP版本管理 服务类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
public interface ISysVersionService extends IService<SysVersion> {

    IPage<SysVersion> managerPage(VersionModel param);
}
