package com.kintreda.common.mybatis.service;

import com.kintreda.common.mybatis.entity.SysAdPosition;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 广告位置 服务类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
public interface ISysAdPositionService extends IService<SysAdPosition> {

    List<SysAdPosition> getAll();

}
