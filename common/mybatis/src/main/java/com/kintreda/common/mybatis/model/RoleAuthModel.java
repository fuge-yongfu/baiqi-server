package com.kintreda.common.mybatis.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@Data
@ApiModel("角色授权参数")
public class RoleAuthModel {

    @ApiModelProperty(value = "角色Id",required = true)
    @NotNull(message = "参数错误")
    private Integer roleId;

    @ApiModelProperty(value = "权限Id")
    private Collection<Integer> roles = new ArrayList<>();

}
