package com.kintreda.common.mybatis.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.kintreda.common.mybatis.entity.SysVersion;
import com.kintreda.common.mybatis.mapper.SysVersionMapper;
import com.kintreda.common.mybatis.model.VersionModel;
import com.kintreda.common.mybatis.service.ISysVersionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * APP版本管理 服务实现类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
@Service
public class SysVersionServiceImpl extends ServiceImpl<SysVersionMapper, SysVersion> implements ISysVersionService {

    /**
     * 版本管理
     * @param param
     * @return
     */
    @Override
    public IPage<SysVersion> managerPage(VersionModel param) {
        IPage<SysVersion> page = new Page<>(param.getPage(),param.getLimit());
        return baseMapper.managerPage(page,param);
    }

}
