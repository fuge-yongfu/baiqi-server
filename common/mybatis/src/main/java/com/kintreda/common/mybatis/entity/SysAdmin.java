package com.kintreda.common.mybatis.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 管理人员
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_admin")
@ApiModel(value="SysAdmin对象", description="管理人员")
public class SysAdmin implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "adm_id", type = IdType.AUTO)
    private Integer admId;

    @ApiModelProperty(value = "姓名",required = true)
    @NotBlank(message = "请输入姓名")
    @TableField("name")
    private String name;

    @ApiModelProperty(value = "用户名",required = true)
    @NotBlank(message = "请输入登录用户名")
    @TableField("username")
    private String username;

    @ApiModelProperty(value = "密码",required = true)
    @NotBlank(message = "请输入登录密码")
    @TableField("password")
    private String password;

    @ApiModelProperty(value = "所属角色",required = true)
    @NotNull(message = "请选择所属身份")
    @TableField("role_id")
    private Integer roleId;

    @ApiModelProperty(value = "状态")
    @TableField("status")
    private Integer status;

    @ApiModelProperty(value = "所属角色名称")
    @TableField(exist = false)
    private String roleName;


}
