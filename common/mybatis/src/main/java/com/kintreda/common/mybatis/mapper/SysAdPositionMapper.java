package com.kintreda.common.mybatis.mapper;

import com.kintreda.common.mybatis.entity.SysAdPosition;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 广告位置 Mapper 接口
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
public interface SysAdPositionMapper extends BaseMapper<SysAdPosition> {

}
