package com.kintreda.common.mybatis.mapper;

import com.kintreda.common.mybatis.entity.SysProtocol;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 协议 Mapper 接口
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
public interface SysProtocolMapper extends BaseMapper<SysProtocol> {

}
