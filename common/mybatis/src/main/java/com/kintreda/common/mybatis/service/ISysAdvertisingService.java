package com.kintreda.common.mybatis.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.mybatis.entity.SysAdvertising;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kintreda.common.mybatis.model.AdvertisingModel;
import com.kintreda.common.mybatis.model.BasePageModel;

/**
 * <p>
 * 广告 服务类
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-18
 */
public interface ISysAdvertisingService extends IService<SysAdvertising> {

    IPage<SysAdvertising> managerPage(AdvertisingModel param);
}
