package com.kintreda.common.mybatis.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kintreda.common.config.R;
import com.kintreda.common.config.constant.RedisConstant;
import com.kintreda.common.config.redis.RedisUtils;
import com.kintreda.common.mybatis.entity.SysConfig;
import com.kintreda.common.mybatis.enums.ConfigGroupEnum;
import com.kintreda.common.mybatis.mapper.SysConfigMapper;
import com.kintreda.common.mybatis.model.ConfigGroupModel;
import com.kintreda.common.mybatis.service.ISysConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 配置信息 服务实现类
 * </p>
 *
 * @author Yongfu
 * @since 2024-01-26
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements ISysConfigService {

    @Resource
    private RedisUtils redisUtils;

    @PostConstruct
    public void SysConfigServiceImpl(){
        String platformName = (String) redisUtils.get(RedisConstant.SYS_CONFIG, "PLATFORM_NAME");
        if (StringUtils.isBlank(platformName)){
            this.createCache();
        }
    }

    /**
     * 根据参数名称获取参数值
     * @param name
     * @return
     */
    @Override
    public String getValueByName(String name) {
        String value = (String) redisUtils.get(RedisConstant.SYS_CONFIG, name);
        if (StringUtils.isNotBlank(value)){
            return value;
        }
        this.createCache();
        return (String) redisUtils.get(RedisConstant.SYS_CONFIG, name);
    }


    /**
     * 获取某个分组的所有配置
     * @param grouping
     * @return
     */
    @Override
    public Map<String, Object> getListByGrouping(ConfigGroupEnum grouping) {
        String group = grouping.getGroup();
        Map<String, Object> all = redisUtils.getAll(RedisConstant.SYS_CONFIG + "_" + group.toLowerCase());
        if (ObjectUtils.isEmpty(all)){
            this.createCache();
        }
        return redisUtils.getAll(RedisConstant.SYS_CONFIG + "_" + group.toLowerCase());
    }


    /**
     * 获取所有配置
     * @return
     */
    @Override
    public R<List<ConfigGroupModel>> allConfig() {
        List<ConfigGroupModel> result = new ArrayList<>();
        //获取配置分组
        List<Map<String, String>> all = ConfigGroupEnum.all();
        all.forEach(x->{
            String group = x.get("group");
            LambdaQueryWrapper<SysConfig> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(SysConfig::getGrouping,group).orderByDesc(SysConfig::getSort);
            List<SysConfig> list = list(queryWrapper);
            if (ObjectUtils.isNotEmpty(list)){
                ConfigGroupModel configGroupModel = new ConfigGroupModel();
                configGroupModel.setName(x.get("name"));
                configGroupModel.setCode(group);
                configGroupModel.setConfigs(list);
                result.add(configGroupModel);
            }
        });

        return R.ok(result);
    }


    /**
     * 生成配置缓存
     */
    @Override
    public void createCache() {
        List<SysConfig> list = list();
        Iterator<SysConfig> iterator = list.iterator();
        Long count = redisUtils.getCount(RedisConstant.SYS_CONFIG);
        if (count>0){
            redisUtils.delete(RedisConstant.SYS_CONFIG);
        }
        while (iterator.hasNext()){
            SysConfig config = iterator.next();
            redisUtils.set(RedisConstant.SYS_CONFIG,config.getName(),config.getValue());
        }
        List<Map<String, String>> all = ConfigGroupEnum.all();
        all.forEach(x->{
            //删掉以前的
            Long group = redisUtils.getCount(RedisConstant.SYS_CONFIG + "_" + x.get("group").toLowerCase());
            if (group>0){
                redisUtils.delete(RedisConstant.SYS_CONFIG+"_"+x.get("group").toLowerCase());
            }
            //重新写缓存
            QueryWrapper<SysConfig> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("grouping",x.get("group"));
            List<SysConfig> itemList = list(queryWrapper);
            if (ObjectUtils.isNotEmpty(itemList)){
                itemList.forEach(c->{
                    redisUtils.set(RedisConstant.SYS_CONFIG+"_"+x.get("group").toLowerCase(),c.getName(),c.getValue());
                });
            }
        });
    }
}
