package com.kintreda.common.mybatis.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.mybatis.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kintreda.common.mybatis.model.UserPageModel;

/**
 * <p>
 * 普通用户 Mapper 接口
 * </p>
 *
 * @author Yongfu
 * @since 2021-01-16
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 用户管理
     * @param page
     * @param param
     * @return
     */
    IPage<SysUser> listByManager(IPage<SysUser> page, UserPageModel param);

    /**
     * 根据用户Id获取用户信息
     * @param userId
     * @return
     */
    SysUser getByUserId(String userId);

    /**
     * 获取微信小程序用户
     * @param openId
     * @return
     */
    SysUser getByWxmpOpenId(String openId);

}
