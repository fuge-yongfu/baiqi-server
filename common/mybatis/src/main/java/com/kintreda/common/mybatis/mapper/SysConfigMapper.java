package com.kintreda.common.mybatis.mapper;

import com.kintreda.common.mybatis.entity.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 配置信息 Mapper 接口
 * </p>
 *
 * @author Yongfu
 * @since 2024-01-26
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
