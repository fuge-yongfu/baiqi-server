package com.kintreda.common.mybatis.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 分页公共参数
 */
@Data
@ApiModel("分页参数")
public class BasePageModel {

    @ApiModelProperty("第几页")
    private Integer page = 1;

    @ApiModelProperty("每页几条")
    private Integer limit = 10;

    @ApiModelProperty("查询关键字")
    private String keyword;

    @ApiModelProperty(value = "所属用户")
    private Long userId;

    @ApiModelProperty(value = "开始日期")
    private String startDate;

    @ApiModelProperty(value = "截止日期")
    private String endDate;

    @ApiModelProperty(value = "指定日期")
    private String date;

    public Integer getPage(){
        if(page==null){
            return 1;
        }
        return page;
    }

    public Integer getLimit(){
        if(limit==null){
            return 10;
        }
        return limit;
    }

}
