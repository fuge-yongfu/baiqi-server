package com.kintreda.common.mybatis.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kintreda.common.mybatis.entity.SysTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kintreda.common.mybatis.model.BasePageModel;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Yongfu
 * @since 2021-07-09
 */
public interface SysTaskMapper extends BaseMapper<SysTask> {

    IPage<SysTask> getList(IPage<SysTask> page, BasePageModel param);
}
