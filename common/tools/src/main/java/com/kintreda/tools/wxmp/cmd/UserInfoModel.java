package com.kintreda.tools.wxmp.cmd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("微信返回用户信息")
public class UserInfoModel {

    @ApiModelProperty(value = "头像")
    private String avatarUrl;

    @ApiModelProperty(value = "OpenId",required = true)
    @NotBlank(message = "登录失败,请重试")
    private String openId;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "手机号")
    private String phoneNumber;

    @ApiModelProperty(value = "性别")
    private Integer gender;

    @ApiModelProperty("国家")
    private String country;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "城市")
    private String city;



}
