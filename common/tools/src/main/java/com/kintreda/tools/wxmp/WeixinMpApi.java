package com.kintreda.tools.wxmp;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.kintreda.common.config.constant.RedisConstant;
import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import com.kintreda.common.config.redis.RedisUtils;
import com.kintreda.tools.wxmp.cmd.UserInfoModel;
import com.kintreda.tools.wxmp.cmd.UserInfoParam;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.core.MediaType;
import java.security.AlgorithmParameters;
import java.security.Security;
import java.util.Arrays;

/**
 * 微信小程序接口
 */
@Component
public class WeixinMpApi {

    @Value("${weixin.mp.appid}")
    public String appid;
    @Value("${weixin.mp.secret}")
    private String secret;

    @Autowired
    private RedisUtils redisUtils;


    /**
     * 获取用户信息
     * @param data
     * @return
     */
    public UserInfoModel getUserInfo(UserInfoParam data){
        try {
            String sessionKey = redisUtils.get(RedisConstant.prefix + "mp_session_key_"+data.getOpenId());
            if (StringUtils.isBlank(sessionKey)){
                throw new CommonBaseException(CommonBaseErrorCode.SYS_ERROR.setErrMsg("授权失败，请重新发起授权!"));
            }
            JSONObject jsonObject = decryptionUserInfo(data.getEncryptedData(), sessionKey, data.getIv());
            UserInfoModel userInfoModel = new UserInfoModel();
            userInfoModel.setAvatarUrl(jsonObject.getString("avatarUrl"));
            userInfoModel.setOpenId(jsonObject.getString("openId"));
            userInfoModel.setNickName(jsonObject.getString("nickName"));
            userInfoModel.setCountry(jsonObject.getString("country"));
            userInfoModel.setProvince(jsonObject.getString("province"));
            userInfoModel.setCity(jsonObject.getString("city"));
            userInfoModel.setGender(jsonObject.getInteger("gender"));
            userInfoModel.setPhoneNumber(jsonObject.getString("phoneNumber"));
            return userInfoModel;
        }catch (CommonBaseException e){
            throw e;
        }catch (Exception e){
            e.printStackTrace();
            throw new CommonBaseException(CommonBaseErrorCode.SYS_ERROR);
        }
    }



    /**
     * 创建会话
     * @param code
     * @return
     */
    public String createSession(String code){
        Client client = Client.create();
        WebResource webResource = client.resource("https://api.weixin.qq.com/sns/jscode2session");
        MultivaluedMapImpl formData = new MultivaluedMapImpl();
        formData.add("appid", appid);
        formData.add("secret", secret);
        formData.add("js_code", code);
        formData.add("grant_type", "authorization_code");
        ClientResponse response = webResource.queryParams(formData).type(MediaType.APPLICATION_FORM_URLENCODED).get(ClientResponse.class);
        String textEntity = response.getEntity(String.class);
        try {
            JSONObject json = JSONObject.parseObject(textEntity);
            if (StringUtils.isNotBlank(json.getString("session_key"))){
                redisUtils.set(RedisConstant.prefix+"mp_session_key_"+json.getString("openid"),json.getString("session_key"),6800);
            }
            String openid = json.getString("openid");
            if(ObjectUtils.isEmpty(openid)){
                throw new CommonBaseException(CommonBaseErrorCode.SYS_ERROR.setErrMsg(json.getString("errmsg")).setCode(json.getInteger("errcode")));
            }
            return openid;
        }catch (JSONException e){
            throw new CommonBaseException(CommonBaseErrorCode.SYS_ERROR);
        }

    }



    /**
     * 小程序解密用户数据
     * @param encryptedData
     * @param sessionKey
     * @param iv
     * @return
     */
    public JSONObject decryptionUserInfo(String encryptedData, String sessionKey, String iv) {
        // 被加密的数据
        byte[] dataByte = Base64.decode(encryptedData);
        // 加密秘钥
        byte[] keyByte = Base64.decode(sessionKey);
        // 偏移量
        byte[] ivByte = Base64.decode(iv);

        try {
            // 如果密钥不足16位，那么就补足. 这个if 中的内容很重要
            int base = 16;
            if (keyByte.length % base != 0) {
                int groups = keyByte.length / base + (keyByte.length % base != 0 ? 1 : 0);
                byte[] temp = new byte[groups * base];
                Arrays.fill(temp, (byte) 0);
                System.arraycopy(keyByte, 0, temp, 0, keyByte.length);
                keyByte = temp;
            }
            // 初始化
            Security.addProvider(new BouncyCastleProvider());
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
            SecretKeySpec spec = new SecretKeySpec(keyByte, "AES");
            AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");
            parameters.init(new IvParameterSpec(ivByte));
            cipher.init(Cipher.DECRYPT_MODE, spec, parameters);// 初始化
            byte[] resultByte = cipher.doFinal(dataByte);
            if (null != resultByte && resultByte.length > 0) {
                String result = new String(resultByte, "UTF-8");
                return JSONObject.parseObject(result);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
