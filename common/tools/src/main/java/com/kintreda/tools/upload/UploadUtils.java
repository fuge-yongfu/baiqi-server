package com.kintreda.tools.upload;

import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import com.kintreda.tools.upload.minio.MinioUtils;
import com.kintreda.tools.utils.AesUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;

/**
 * 文件上传工具
 */
@Component
public class UploadUtils {

    private static String uploadType;
    @Value("${platform.upload}")
    public void setUploadType(String uploadType) {
        UploadUtils.uploadType = uploadType;
    }
    private static UploadUtils uploadUtils;
    private static QiniuUpload qiniuUpload;
    private static AliyunOssUtils aliyunOssUtils;
    private static MinioUtils minioUtils;
    @Autowired
    private QiniuUpload qiniu;
    @Autowired
    private AliyunOssUtils aliyun;
    @Autowired
    private MinioUtils minio;

    @PostConstruct
    public void init(){
        UploadUtils.uploadUtils = this;
        uploadUtils.qiniuUpload = qiniu;
        uploadUtils.aliyunOssUtils = aliyun;
        uploadUtils.minioUtils = minio;
    }


    /**
     * 文件上传
     * @param file
     * @return
     */
    public static String doUp(MultipartFile file) {
        String res = "";
        //上传到七牛
        if (uploadType.equals("qiniu")){
            res = uploadUtils.qiniuUpload.upload(file);
        }
        //上传阿里云
        if (uploadType.equals("aliyun")){
            res = uploadUtils.aliyunOssUtils.upload(file);
        }
        //minio上传
        if (uploadType.equals("minio")){
            res = uploadUtils.minioUtils.upload(file);
        }
        //上传本地
        if (uploadType.equals("local")){
            throw new CommonBaseException(CommonBaseErrorCode.SYS_ERROR);
        }
        return res;
    }
}
