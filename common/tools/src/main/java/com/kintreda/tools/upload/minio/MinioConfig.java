package com.kintreda.tools.upload.minio;

import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MinioConfig {

    @Value("${upload.minio.accessKey}")
    private String accessKey;
    @Value("${upload.minio.secretKey}")
    private String secretKey;
    @Value("${upload.minio.endpoint}")
    private String endpoint;


    @Bean("minioClient")
    public MinioClient minioClient(){
        return MinioClient.builder()
                .endpoint(endpoint)
                .credentials(accessKey, secretKey)
                .build();
    }


}
