package com.kintreda.tools.wxmp.cmd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("解密用户参数")
public class UserInfoParam {

    @ApiModelProperty(value = "encryptedData",required = true)
    @NotBlank(message = "授权失败,请重新发起授权")
    private String encryptedData;

    @ApiModelProperty(value = "iv",required = true)
    @NotBlank(message = "授权失败,请重新发起授权")
    private String iv;

    @ApiModelProperty(value = "openId",required = true)
    @NotBlank(message = "会话过期,请重新授权!")
    private String openId;


}
