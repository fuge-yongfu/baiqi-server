package com.kintreda.common.config.constant;

/**
 * Redis常量
 */
public class RedisConstant {

    public static final String prefix = "k_";

    /**TOKEN**/
    public static final String TOKEN = prefix + "jwt_token_";
    /**验证码**/
    public static final String VERIFY_CODE = prefix+"verify_code_";
    /**系统设置参数**/
    public static final String SYS_CONFIG = prefix + "sys_config";


}
