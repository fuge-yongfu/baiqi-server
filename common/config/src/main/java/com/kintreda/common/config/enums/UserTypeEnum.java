package com.kintreda.common.config.enums;

import lombok.Getter;

/**
 * 用户类型
 */
@Getter
public enum UserTypeEnum {

    后台管理员("admin",null),
    用户("user",3);

    private String type;
    private Integer roleId;

    UserTypeEnum(String type,Integer roleId){
        this.type = type;
        this.roleId = roleId;
    }

}
