package com.kintreda.common.config.exception;

import lombok.Data;

/**
 * @author zhou_shun
 * @date 2019-10-29 下午 15:52
 */

@Data
public class CommonBaseException extends RuntimeException {


    private Integer code;
    private String errMsg;

    private CommonBaseException(Integer code, String errMsg){
        super(errMsg);
        this.code = code;
        this.errMsg = errMsg;
    }

    public CommonBaseException(CommonBaseErrorCode error){
        this(error.getCode(),error.getErrMsg());
    }

    public CommonBaseException(CommonBaseErrorCode error, String msg){
        this(error.getCode(), msg);
    }
    public CommonBaseException(CommonBaseErrorCode error, Integer code){
        this(code,error.getErrMsg());
    }

}
