package com.kintreda.common.config.exception;

/**
 * 错误代码
 * @author zhou_shun
 * @date 2019-10-29 下午 15:44
 */

public enum CommonBaseErrorCode {

    //系统
    NOT_AUTH(403,"您没有访问权限"),
    LOGIN_OVERTIME(10000,"登录身份过期,请重新登录"),
    SYS_ERROR(1000,"系统繁忙，请稍后重试！"),   //系统错误
    SYS_PARAM(1001,"参数错误，请检查内容！"),   //参数错误
    TOKEN_FAIL(1002,"非法请求,请重新登录"),
    INCORRECT_USERNAME_OR_PASSWORD(1003,"账号或密码错误"),
    PLEASE_LOGIN_AGAIN(1004, "请重新登录"),
    ILLEGAL_TOKEN(1005, "非法的token"),
    SYS_VERIFY_ERROR(1006,"验证码错误"),
    SYS_MESSAGE_SENDFAIL(1007,"消息发送失败"),
    ACCOUNT_REPETITION(1008,"账户重复不可用"),
    ACCOUNT_LOGIN_CLOSE(1009,"此账户暂时无法登录,请联系管理员"),
    DEL_PERMISSION_FAIL(1010,"不能删除此项目!"),
    DEL_FAIL(1011,"删除失败,请稍后重试!"),
    ROLE_AUTH_FAIL(1012,"角色授权失败,请稍后重试!"),
    USER_LOGIN_FAIL(1013,"登录失败,请检查账户"),
    ACCOUNT_NOT_AUTH(1014,"没有账户操作权限"),
    DATA_IS_NULL(1015,"数据不存在"),
    ORGAN_ADMIN_BIND_FAIL(1016,"用户已经绑定过其他机构,无法绑定多个机构"),

    //上传
    USER_PICTURE_FAIL(2000,"文件上传失败"),
    USER_PICTURE_OVERSIZE(2001,"上传文件过大"),
    USER_PICTURE_FORMAT_ERROR(2002,"上传文件格式不正确"),
    USER_PICTURE_IS_EMPTY(2003,"请选择要上传的文件"), ;



    private Integer code;
    private String errMsg;

    public Integer getCode() {
        return code;
    }

    public CommonBaseErrorCode setCode(Integer code) {
        this.code = code;
        return this;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public CommonBaseErrorCode setErrMsg(String errMsg) {
        this.errMsg = errMsg;
        return this;
    }

    public CommonBaseErrorCode setErrMsgPlaceholder(String errMsg) {
        this.errMsg = String.format(this.getErrMsg(),errMsg);
        return this;
    }


    CommonBaseErrorCode(Integer code){
        this.code = code;
        this.errMsg = errMsg;
    }

    CommonBaseErrorCode(Integer code, String errMsg){
        this.code = code;
        this.errMsg = errMsg;
    }

}
