package com.kintreda.common.config.exception;

import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.kintreda.common.config.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.util.List;

/**
 * 自定义异常类
 * @author zhou_shun
 * @date 2019-10-29 下午 15:40
 */

@Slf4j
@ResponseBody
@ControllerAdvice
@RestControllerAdvice
@Component
public class GlobalException {

    /**
     * 自定义异常
     * @param e
     * @return
     */
    @ExceptionHandler(CommonBaseException.class)
    public R commonBaseException(CommonBaseException e) {
        return R.err(e);
    }

    @ExceptionHandler
    public R constraintViolationException(ConstraintViolationException e) {
        return R.err(new CommonBaseException(CommonBaseErrorCode.SYS_ERROR,e.getMessage()));
    }

    /**
     *  参数校验
     * @param e
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public R methodArgumentNotValidException(MethodArgumentNotValidException e) {
        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
        log.error(CommonBaseErrorCode.SYS_PARAM.getCode().toString(), e);
        StringBuffer sb = new StringBuffer();
        if(!ObjectUtils.isEmpty(fieldErrors)){
            sb.append(fieldErrors.get(0).getDefaultMessage());
        }
        CommonBaseException commonBaseException = new CommonBaseException(CommonBaseErrorCode.SYS_PARAM,sb.toString());
        return R.err(commonBaseException);
    }

    @ExceptionHandler(MismatchedInputException.class)
    public R MismatchedInputException(MismatchedInputException e){
        return R.err(new CommonBaseException(CommonBaseErrorCode.SYS_ERROR,e.getMessage()));
    }

    @ExceptionHandler
    public R BindException(BindException e){
        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
        log.error(CommonBaseErrorCode.SYS_PARAM.getCode().toString(), e);
        StringBuffer sb = new StringBuffer();
        if(!ObjectUtils.isEmpty(fieldErrors)){
            sb.append(fieldErrors.get(0).getDefaultMessage());
        }
        CommonBaseException commonBaseException = new CommonBaseException(CommonBaseErrorCode.SYS_PARAM,sb.toString());
        return R.err(commonBaseException);
    }

    /**
     * Exception
     * @param e
     * @return
     */
    @ExceptionHandler
    public R exception(Exception e) {
        e.printStackTrace();
        return R.err(new CommonBaseException(CommonBaseErrorCode.SYS_ERROR));
    }
}
