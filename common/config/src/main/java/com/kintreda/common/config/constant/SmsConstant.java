package com.kintreda.common.config.constant;

/**
 * 短信通知常量
 */
public class SmsConstant {


    public static final String NOTE_SIGN = "【晶创达科技】";
    public static String MESSAGE_TEMP_CODE = "您的验证码是：%s，有效期5分钟，过期无效.";
}
