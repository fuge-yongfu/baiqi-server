package com.kintreda.common.config;

import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 公共返回结构
 */

@Data
@ApiModel("公共返回结构")
public class R<T> {

    @ApiModelProperty("返回代码")
    private Integer code;

    @ApiModelProperty("消息")
    private String message;

    @ApiModelProperty("返回数据")
    private T data;

    public R(){

    }
    public R(Integer code, String message){
        this.code = code;
        this.message = message;
    }
    public R(Integer code, String message, T data){
        this(code, message);
        this.data = data;
    }

    public static<T> R ok(){
        return new R(200,"success");
    }

    public static<T> R ok(T data){
        return new R(200,"success",data);
    }

    public static R err(Integer code, String message){
        return new R(code, message);
    }

    //错误异常代码
    public static R err(CommonBaseException e){
        return new R(e.getCode(), e.getMessage());
    }

    public static R err(CommonBaseErrorCode code){
        return new R(code.getCode(), code.getErrMsg());
    }

}
