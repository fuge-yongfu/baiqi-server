package com.kintreda.common.config.exception;

/**
 * 参数验证
 * @author zhou_shun
 * @date 2019-11-15 下午 22:27
 */
public class ArgVerify {

    public static void verify(String string, String errMessage){
        if(null == string || string.trim().length() == 0){
            throw new CommonBaseException(CommonBaseErrorCode.SYS_PARAM.setErrMsg(errMessage));
        }
    }
}
