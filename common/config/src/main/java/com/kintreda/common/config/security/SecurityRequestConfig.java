package com.kintreda.common.config.security;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Security默认的拦截配置
 */
public class SecurityRequestConfig {

    /**
     * 安全请求
     */
    public List<String> securityRequestList = new ArrayList<>();


    /**
     * 初始化安全请求
     */ {
        //验证码
        securityRequestList.add("/captcha/**");
        securityRequestList.add("/verify/**");
        //API接口放行
        securityRequestList.add("/api/protocol/**");
        securityRequestList.add("/api/advert/**");
        securityRequestList.add("/api/feedback/**");
        securityRequestList.add("/api/version/**");

        securityRequestList.add("/common/**");
        securityRequestList.add("/api/login/**");
        securityRequestList.add("/platform/login/**");
        securityRequestList.add("/error");

        //放行 swagger-ui
        securityRequestList.add("/doc.html/**");
        securityRequestList.add("/swagger-ui.html/**");
        securityRequestList.add("/swagger-resources/**");
        securityRequestList.add("/webjars/**");
        securityRequestList.add("/v2/**");
        securityRequestList.add("/resources/**");
//        securityRequestList.add("/redis/**");

        //放行监控
        securityRequestList.add("/favicon.ico");
        securityRequestList.add("/monitor/**");
    }


}
