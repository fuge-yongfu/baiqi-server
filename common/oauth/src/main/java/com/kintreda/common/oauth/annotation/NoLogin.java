package com.kintreda.common.oauth.annotation;

import java.lang.annotation.*;

/**
 * 标注此注解的controller不需要登陆
 * @author 永福
 */
@Documented
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface NoLogin {

}
