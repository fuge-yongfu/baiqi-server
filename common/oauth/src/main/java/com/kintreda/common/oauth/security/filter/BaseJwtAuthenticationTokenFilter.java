package com.kintreda.common.oauth.security.filter;


import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.kintreda.common.config.exception.CommonBaseException;
import com.kintreda.common.oauth.security.service.UserService;
import com.kintreda.common.oauth.jwt.JwtConfig;
import com.kintreda.common.oauth.jwt.JwtUtils;
import io.jsonwebtoken.Claims;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * User: jane
 * Date: 2020/1/5
 * Time: 5:12 下午
 * token的过滤器.
 * 解析请求头中的token->解析token得到用户信息->丢到SecurityContextHolder中
 * <p>
 * 2020年01月09日23:07:56  此处注释掉@Component的原因: 加上这个注解会造成拦截两次的问题
 * 可以在44行加入 ' log.info("authHeader->>    {}",authHeader);' 自行测试
 */
@Slf4j
public class BaseJwtAuthenticationTokenFilter extends OncePerRequestFilter {

    UserService userService;


    public BaseJwtAuthenticationTokenFilter(UserService userService) {
        this.userService = userService;
    }

    @SneakyThrows
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) {
        response.setHeader("Access-control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PUT,DELETE");
        response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
        // 跨域时会首先发送一个option请求，这里我们给option请求直接返回正常状态
        if (request.getMethod().equals(RequestMethod.OPTIONS.name())) {
            response.setStatus(HttpStatus.OK.value());
            return;
        }
        String token = request.getHeader(JwtConfig.HEADER);
        String requestURI = request.getRequestURI();
        log.info("doFilterInternal-->requestURI-->>  {}", requestURI);
        log.info("authHeader-user_token->>  {}", token);
        if (StrUtil.isNotBlank(token)) {
            Claims claimsFromToken;
            try {
                claimsFromToken = JwtUtils.getClaimsFromToken(token);
            } catch (CommonBaseException e) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("code",e.getCode());
                jsonObject.put("message",e.getErrMsg());
                response.setCharacterEncoding("utf-8");
                response.setContentType("text/javascript;charset=utf-8");
                response.getWriter().print(jsonObject);
                return;
            }
            String userType = claimsFromToken.get("userType", String.class);
            String userId = claimsFromToken.get("userId", String.class);

            if (userType != null && userId!=null && SecurityContextHolder.getContext().getAuthentication() == null) {
                UserDetails userDetails = userService.loadUserByUsername(userType+":"+userId);
                if (userDetails != null) {
                    // 校验token
                    boolean verification = JwtUtils.verification(token);
                    if (verification) {
                        UsernamePasswordAuthenticationToken authentication =
                                new UsernamePasswordAuthenticationToken(
                                        userDetails,
                                        userDetails.getPassword(),
                                        userDetails.getAuthorities()
                                );
                        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                        SecurityContextHolder.getContext().setAuthentication(authentication);
                    }
                }
            }
        }
        chain.doFilter(request, response);
    }
}
