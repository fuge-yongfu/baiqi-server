package com.kintreda.common.oauth.security.handler;


import com.alibaba.fastjson.JSONObject;
import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Created with IntelliJ IDEA.
 * User: jane
 * Date: 2020/1/5
 * Time: 4:49 下午
 *
 * @Description: 未携带token用户, 或者非法token访问无权限资源时的异常
 */
@Slf4j
@Component
public class BaseAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @SneakyThrows
    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse response, AuthenticationException e) {

        String requestURI = httpServletRequest.getRequestURI();
        log.info("BaseAuthenticationEntryPoint-->>   {}", requestURI);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",CommonBaseErrorCode.LOGIN_OVERTIME.getCode());
        jsonObject.put("message",CommonBaseErrorCode.LOGIN_OVERTIME.getErrMsg());

        response.setCharacterEncoding("utf-8");
        response.setContentType("text/javascript;charset=utf-8");
        response.getWriter().print(jsonObject);


    }
}
