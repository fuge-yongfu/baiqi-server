package com.kintreda.common.oauth.jwt;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * JWT配置
 */
@Component
public class JwtConfig {

    /**
     * 表单字段
     */
    public static String HEADER = "Authorization";

    /**
     * 令牌前缀
     */
    public static String PREFIX = "Kintreda";

    /**
     * 令牌
     */
    public static String SECRET;

    /**
     * 令牌过期时间 此处单位/毫秒
     */
    public static long EXPIRE_TIME = 1 * 3600 * 1000;//86400000


    @Value("${jwt.header}")
    public void setHEADER(String val){
        JwtConfig.HEADER = val;
    }

    @Value("${jwt.prefix}")
    public void setPREFIX(String val){
        JwtConfig.PREFIX = val;
    }

    @Value("${jwt.secret}")
    public void setSECRET(String val){
        JwtConfig.SECRET = val;
    }

    @Value("${jwt.expire}")
    public void setExpireTime(long val){
        JwtConfig.EXPIRE_TIME = val * 3600 * 1000;
    }

}
