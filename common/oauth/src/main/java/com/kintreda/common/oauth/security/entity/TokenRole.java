package com.kintreda.common.oauth.security.entity;

import com.kintreda.common.mybatis.entity.SysRole;
import com.kintreda.common.mybatis.entity.SysRoleMenu;
import org.springframework.security.core.GrantedAuthority;

public class TokenRole extends SysRole implements GrantedAuthority {

    @Override
    public String getAuthority() {
        return super.getRoleId().toString();
    }
}
