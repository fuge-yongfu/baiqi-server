package com.kintreda.common.oauth.security.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@Builder
public class TokenUser implements UserDetails, Serializable {

    private int userId;

    private String userType;

    private String nickName;

    private String username;

    private String password;

    private List<TokenRole> roles;

    public TokenUser(Integer userId, String userType,String nickName, String username,String password,List<TokenRole> roles){
        this.userId = userId;
        this.userType = userType;
        this.nickName = nickName;
        this.username = username;
        this.password = password;
        this.roles = roles;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<SimpleGrantedAuthority> auths = new ArrayList<>();
        roles.forEach(o -> auths.add(new SimpleGrantedAuthority(o.getAuthority())));
        return auths;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
