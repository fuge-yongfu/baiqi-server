package com.kintreda.common.oauth.security.handler;


import com.alibaba.fastjson.JSONObject;
import com.kintreda.common.config.R;
import com.kintreda.common.config.exception.CommonBaseErrorCode;
import com.kintreda.common.config.exception.CommonBaseException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Created with IntelliJ IDEA.
 * User: jane
 * Date: 2020/1/5
 * Time: 5:05 下午
 * 没有权限,被拒绝访问时的调用类
 */
@Component
@Slf4j
public class BaseRestAccessDeniedHandler implements AccessDeniedHandler {

    @SneakyThrows
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",CommonBaseErrorCode.NOT_AUTH.getCode());
        jsonObject.put("message",CommonBaseErrorCode.NOT_AUTH.getErrMsg());

        response.setCharacterEncoding("utf-8");
        response.setContentType("text/javascript;charset=utf-8");
        response.getWriter().print(jsonObject);
    }
}
