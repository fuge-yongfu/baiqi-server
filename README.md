#### 介绍

一个基于springboot的纯净版脚手架，让你快速搭建自己的系统，没有多余的东西，适合初学者快速起步项目~

- 这是服务端项目，提供服务端相关功能
- 前端界面请访问：[ADMIN-UI](https://gitee.com/fuge-yongfu/baiqi-admin-ui) 

#### 在线体验

-   [演示地址](https://demo.baiqi.cicms.cn/) (账号：admin 密码：admin)
-   [文档地址](https://baiqi.cicms.cn/) (文档还在完善中...)

#### 软件架构

- 后台采用SpringBoot，MyBatis-Plus，JWT，Redis等技术
- 数据库采用mysql
- 前端采用vue3
- 前端采用 [ArcoDesign组件库](https://arco.design/vue/docs/start)
- 验证码采用 [AJ-Captcha](https://gitee.com/anji-plus/captcha )

#### 功能截图

<div style="width:100%;display: flex;flex-direction: row;justify-content: space-between">
<img src="./temp/1.png" width="48%" />
<img src="./temp/2.png" width="48%" />
</div>
<div style="width:100%;display: flex;flex-direction: row;justify-content: space-between;margin-top: 30px">
<img src="./temp/3.png" width="48%" />
<img src="./temp/4.png" width="48%" />
</div>

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

